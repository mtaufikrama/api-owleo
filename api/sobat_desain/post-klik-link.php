<?php
include "../token/cek-no-token.php";

// id_olshop_product, device_data

if (empty($id_olshop_product)) {
	$datax['code'] = 500;
	$datax['msg'] = "Olshop produk tidak ada";
	echo encryptData($datax);
	die();
}

$klikLink['id'] = generateID('click_olshop_product_affiliate', 'id');
$klikLink['id_olshop_product_affiliate'] = $id_olshop_product;
$klikLink['device_data'] = is_array($device_data) ? json_encode($device_data) : null;
$klikLink['waktu'] = date("Y-m-d H:i:s");

$result = insert_tabel('click_olshop_product_affiliate', $klikLink);

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Produk";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Produk";
}
echo encryptData($datax);

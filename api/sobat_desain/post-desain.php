<?php
include "../token/cek-token-sobat-desain.php";

// id, nama, jenis_produk, makna

// jenis_produk = id, id_jenis_produk

if (empty($nama)) {
	$datax['code'] = 500;
	$datax['msg'] = "Nama Produk tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('desain_sobat_desain', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

$dataDesainSobatDesain['id_user'] = $id_user;
$dataDesainSobatDesain['nama'] = $nama;
$dataDesainSobatDesain['makna'] = $makna;
$dataDesainSobatDesain['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$id = generateID('desain_sobat_desain', 'id');
	$dataDesainSobatDesain['id'] = $id;
	$result = insert_tabel('desain_sobat_desain', $dataDesainSobatDesain);
} else {
	$result = update_tabel('desain_sobat_desain', $dataDesainSobatDesain, "where binary id='$id'");
}

if ($result) {
	foreach ($jenis_produk as $jenis) {
		$id_jenis_produk_desain_sobat_desain = $jenis['id'];
		$jenisProdukDesainSobatDesain['id_desain_sobat_desain'] = $id;
		$jenisProdukDesainSobatDesain['id_jenis_produk'] = $jenis['id_jenis_produk'];
		$jenisProdukDesainSobatDesain['waktu'] = date("Y-m-d H:i:s");

		if (empty($jenisProdukDesainSobatDesain['link'])) {
			if (empty($id_jenis_produk_desain_sobat_desain)) {
				$jenisProdukDesainSobatDesain['id'] = generateID('jenis_produk_desain_sobat_desain', 'id');
				if ($result) $result = insert_tabel('jenis_produk_desain_sobat_desain', $jenisProdukDesainSobatDesain);
			} else {
				$cekID = baca_tabel('jenis_produk_desain_sobat_desain', 'count(*)', "where binary id = '$id'");
				if ($cekID > 0) {
					if ($result) $result = update_tabel('jenis_produk_desain_sobat_desain', $jenisProdukDesainSobatDesain, "where binary id='$id_jenis_produk_desain_sobat_desain'");
				}
			}
		}
	}
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Produk";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Produk";
}
echo encryptData($datax);

<?php
include "../token/cek-token-mitra.php";

// nama, makna, id_jenis_produk

$input = [
    "nama" => "Fvcking Tale",
    "makna" => "makna",
    "jenis_produk" => ["id_jenis_produk", "id_jenis_produk"],
];

$id_produk = generateID('produk', 'id');
$dataProduk['id'] = $id_produk;
$dataProduk['id_user'] = $id_user;
$dataProduk['nama'] = $nama;
$dataProduk['makna'] = $makna;
$result = insert_tabel('produk', $dataProduk);

foreach ($jenis_produk as $id_jenis_produk) {
    $id_produk_berdasarkan_jenis = generateID('produk_berdasarkan_jenis', 'id');
    $dataJenisProduk['id'] = $id_produk_berdasarkan_jenis;
    $dataJenisProduk['id_user'] = $id_user;
    $dataJenisProduk['id_produk'] = $id_produk;
    $dataJenisProduk['id_jenis_produk'] = $id_jenis_produk;
    $dataJenisProduk['waktu'] = date('Y-m-d H:i:s');
    $result = insert_tabel('produk_berdasarkan_jenis', $dataJenisProduk);
}

if ($result) {
    $datax['code'] = 200;
    $datax['msg'] = 'Berhasil Mengupload Produk';
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Gagal Mengupload Produk";
}
echo encryptData($datax);

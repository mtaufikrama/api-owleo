<?php
include "../token/cek-token-mitra.php";

// id_produk

$sql = "SELECT id as id_produk, nama as nama_produk, makna
    FROM produk
    WHERE binary id='$id_produk' 
    and binary id_user='$id_user'";

unset($id_produk);

$run = $db->Execute($sql);

$produk = [];

while ($get = $run->fetchRow()) {
    $id_produk = $get['id_produk'];
    $sqlProdukJenis = "SELECT id as id_produk_berdasarkan_jenis, b.nama as nama_jenis
        FROM produk_berdasarkan_jenis a
        LEFT JOIN jenis_produk b ON a.id_jenis_produk=a.id
        WHERE binary a.id_produk='$id_produk' 
        and binary a.id_user='$id_user'
        order by a.waktu desc limit 1";

    $runProdukJenis = $db->Execute($sqlProdukJenis);

    $total = 0;
    while ($getProdukJenis = $runProdukJenis->fetchRow()) {
        $id_produk_berdasarkan_jenis = $getProdukJenis['id_produk_berdasarkan_jenis'];
        $sqlPenjualan = "SELECT a.id as id_penjualan_produk_olshop, b.nama as nama_olshop, a.total, c.link
            FROM penjualan_produk_olshop a
            LEFT JOIN olshop b ON a.id_olshop=b.id
            LEFT JOIN link_produk c ON a.id_produk_berdasarkan_jenis=c.id_produk_berdasarkan_jenis AND b.id=c.id_olshop
            WHERE binary a.id_produk_berdasarkan_jenis='$id_produk_berdasarkan_jenis' 
            order by a.waktu desc";

        $runPenjualan = $db->Execute($sqlPenjualan);

        $total_olshop = 0;
        while ($getPenjualan = $runPenjualan->fetchRow()) {
            $jumlah = round($getPenjualan['total']);
            $total_olshop += $jumlah;
            $getPenjualan['total'] = $jumlah;
            $penjualan_produk_olshop[] = $getPenjualan;
        }

        $getProdukJenis['total_olshop'] = $total_olshop;
        $getProdukJenis['penjualan_produk'] = $penjualan_produk_olshop;

        unset($penjualan_produk_olshop);

        $total += $total_olshop;
        $produk_berdasarkan_jenis[] = $getProdukJenis;
    }

    $get['total'] = $total;
    $get['produk_berdasarkan_jenis'] = $produk_berdasarkan_jenis;

    unset($produk_berdasarkan_jenis);

    $produk = $get;
}

if ($produk) {
    $datax['code'] = 200;
    $datax['msg'] = 'Berhasil';
    $datax['penjualan_produk'] = $produk;
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Gagal Mengupload Kritik Saran";
}
echo encryptData($datax);

<?php
ini_set('max_execution_time', '300');
date_default_timezone_set('UTC');
header("Content-Type: text/plain; charset=utf-8");

include "../../_lib/function/db_login.php";
include "../../_lib/function/function.olah_tabel.php";

include "../function/date_time.php";
include "../function/random_string.php";
include "../function/image_link.php";
include "../function/encrypt_data.php";
include "../function/like_text.php";
include "../function/user.php";
include "../function/activity_user.php";
include "../function/send-email.php";
include "../function/get_all_data.php";

// include "jwt.php";
// include "../encrypt.php";
// include "../../_lib/function/function.max_kode_number.php";
// include "../../_lib/function/function.angka.php";
// include "../../_lib/function/function.form.php";
// include "../../_lib/function/function.angka_romawi.php";
// include "../../_lib/function/function.max_kode_text.php";
// include "../../_lib/function/function.uang.php";
// include "../../_lib/function/function.variabel.php";
// include "../../_lib/function/variabel.php";
// header('Content-Type: application/json; charset=utf-8');


// // Izinkan metode HTTP yang diperlukan
// header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");

// // Izinkan header yang diperlukan dalam permintaan
// header("Access-Control-Allow-Headers: Content-Type");

// // Izinkan pengiriman kredensial (jika diperlukan)
// header("Access-Control-Allow-Credentials: true");

// // Perlakukan permintaan OPTIONS sebagai permintaan pra-penerbangan dan atur waktu kadaluwarsa cache
// if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
//     header("Access-Control-Max-Age: 1728000");
//     header("Content-Length: 0");
//     header("Content-Type: text/plain");
// }
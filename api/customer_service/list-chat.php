<?php
include "../token/cek-token-cs.php";

$sql = "SELECT a.id_user, b.pesan, a.waktu
    FROM chat a
    LEFT JOIN chat_detail b ON a.id=b.id_chat
    WHERE binary a.id_user_admin is null 
    and a.is_finish <> 0
    ORDER BY b.waktu DESC";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
    $id_userz = $get['id_user'];
    unset($get['id_user']);
    $get['user'] = get_user($id_userz);
    $get['waktu'] = date_time($get['waktu'], $authorized);
    $chat[] = $get;
}

if ($chat) {
    $datax['code'] = 200;
    $datax['msg'] = 'Berhasil';
    $datax['chat'] = $chat;
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Gagal Mengupload Kritik Saran";
}
echo encryptData($datax);

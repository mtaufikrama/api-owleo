<?php
include "../token/cek-token-cs.php";

// id_chat

$cekBanyakChat = baca_tabel('chat','count(*)',"where id_customer_service='$id_customer_service' and is_finish <> 1");

if ($cekBanyakChat > 5) {
    $datax['code'] = 500;
    $datax['msg'] = "Hanya 5 customer yang dapat kamu tangani";
    echo encryptData($datax);
    die();
}

$chat['id_customer_service'] = $id_customer_service;

$result = update_tabel('chat', $chat, "where id_customer_service is null and id='$id_chat'");

if ($result) {
    $cek = baca_tabel('chat', 'count(*)', "where id_chat='$id_chat' and id_customer_service='$id_customer_service'");
    if ($cek > 0) {
        $nama_cs = baca_tabel('customer_service a join user b on a.id_user=b.id', 'b.nama', " where a.id='$id_customer_service'");
        $chat_detail['id'] = generateID('chat_detail', 'id');
        $chat_detail['id_chat'] = $id_chat;
        $chat_detail['id_user'] = $id_customer_service;
        $chat_detail['pesan'] = 'Selamat Malam kak, Perkenalkan nama saya blablabla';
        $chat_detail['waktu'] = date('Y-m-d H:i:s');
        $result = insert_tabel('chat_detail', $chat_detail);
        $datax['code'] = 200;
        $datax['msg'] = 'Berhasil';
    } else {
        $datax['code'] = 500;
        $datax['msg'] = "Maaf, Sudah ditangani oleh CS lain";
    }
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Maaf Terjadi Kesalahan, Silahkan Coba Lagi";
}
echo encryptData($datax);

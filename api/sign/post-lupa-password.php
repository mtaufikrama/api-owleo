<?php

include '../token/cek-no-token.php';

//email

if (empty($email)) {
    $datax['code'] = 404;
    $datax['msg'] = 'Email Tidak Ada';
    echo encryptData($datax);
    die();
}

$result = delete_tabel('otp', "where email='$email'");

$otp = randomOtp();
$id_otp = generateID('otp', 'id');

$dataOtp['id'] = $id_otp;
$dataOtp['send_email_name'] = 'lupa-pass';
$dataOtp['email'] = $email;
$dataOtp['otp'] = $otp;
$dataOtp['durasi'] = 5;
$dataOtp['waktu'] = date("Y-m-d H:i:s");

if ($result) $result = insert_tabel('otp', $dataOtp);

if (!$result) {
    $datax['code'] = 500;
    $datax['msg'] = 'Terjadi Kesalahan, Silahkan Resend Code Kembali';
    echo encryptData($datax);
    die();
}

// $datax['code'] = 200;
// $datax['msg'] = 'Kode OTP telah dikirimkan';

$cek = baca_tabel('user', 'count(*)', "where email='$email'");

if ($cek <= 0) {
    $datax['code'] = 500;
    $datax['msg'] = 'Email tidak terdaftar';
    echo encryptData($datax);
    die();
}

$nama = baca_tabel('user', 'nama', "where email='$email'");
// $pw = randomString();
// $pw_baru = base64_encode(enkrip($pw));

// $data['password'] = $pw_baru;

// $result = update_tabel('user', $data, "where email='$email_user' and password='$password'");

$params['nama_pengguna'] = $nama;
$params['otp'] = $otp;

$datax['code'] = 200;
$datax['msg'] = 'Silahkan Periksa ke Email untuk Ubah Password';

echo encryptData($datax);

send_email('lupa-pass', $email, $params);

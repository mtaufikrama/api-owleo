<?php

include '../token/cek-no-token.php';

// $db->debug = true;

// username, password, device_data, gmt

if (empty($username) || empty($password)) {
    $datax['code'] = 500;
    $datax['msg'] = 'Email tidak terdaftar';
    echo encryptData($datax);
    die();
}

unset($data);

$pass = base64_encode(enkrip($password));

$kondisi = "email = '$username' or username = '$username'";
$cekemail = baca_tabel('user', 'count(*)', 'where ' . $kondisi);

if ($cekemail > 0) {
    $cek = baca_tabel('user', "count(*)", "where ($kondisi) and password = '$pass'");
    if ($cek > 0) {
        $available = baca_tabel('user', "available", "where ($kondisi) and password = '$pass'");
        $json_device = json_encode($device_data);
        $id_user = baca_tabel('user', 'id', "where ($kondisi) and password = '$pass'");
        if ($available == 1) {
            $id_roles = baca_tabel('user', 'id_roles', "where binary id='$id_user'");
            $token = baca_tabel('login', 'token', "where binary id_user = '$id_user' and device_data = '$json_device'");
            $email = baca_tabel('user', "email", "where ($kondisi) and password = '$pass'");
            if (empty($token)) {
                $token = generateToken();
                $data['id_user'] = $id_user;
                $data['token'] = $token;
                $data['device_data'] = $json_device;
                $data['gmt'] = $gmt;
                $data['waktu'] = date("Y-m-d H:i:s");
                $insert = insert_tabel('login', $data);

                $params = location_for_email($token);
                $params['nama_pengguna'] = baca_tabel('user', 'nama', "where binary id = '$id_user'");

                if ($insert) {
                    $datax['code'] = 200;
                    $datax['msg'] = 'Berhasil Login';
                    $datax['token'] = $data['token'];
                    $datax['username'] = baca_tabel('user', 'username', "where binary id='$id_user'");
                    $datax['roles'] = baca_tabel('roles', 'nama_roles', "where binary id='$id_roles'");
                } else {
                    $datax['code'] = 500;
                    $datax['msg'] = 'Login Gagal';
                }
            } else {
                $data['waktu'] = date("Y-m-d H:i:s");

                $params = location_for_email($token);
                $params['nama_pengguna'] = baca_tabel('user', 'nama', "where binary id = '$id_user'");

                $update = update_tabel('login', $data, "where binary id_user = '$id_user' and device_data = '$json_device'");

                if ($update) {
                    $datax['code'] = 200;
                    $datax['msg'] = 'Berhasil Login';
                    $datax['token'] = $token;
                    $datax['username'] = baca_tabel('user', 'username', "where binary id='$id_user'");
                    $datax['roles'] = baca_tabel('roles', 'nama_roles', "where binary id='$id_roles'");
                } else {
                    $datax['code'] = 500;
                    $datax['msg'] = 'Login Gagal';
                }
            }
        } else {
            $datax['code'] = 505;
            $datax['msg'] = 'Akun kamu belum terverifikasi';
        }
    } else {
        $datax['code'] = 500;
        $datax['msg'] = 'Password Salah';
    }
} else {
    $datax['code'] = 500;
    $datax['msg'] = 'Email tidak terdaftar';
}

echo encryptData($datax);

if ($datax['code'] == 200) send_email('login-baru', $email, $params);

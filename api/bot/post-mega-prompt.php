<?php
include "../token/cek-token-admin.php";

// id, id_roles, id_jenis_bot, mega_prompt

if (!empty($mega_prompt)) $dataMegaPrompt['mega_prompt'] = $mega_prompt;

if (!empty($id)) {
	$cekID = baca_tabel('mega_prompt', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id_jenis_bot)) {
	$cekID = baca_tabel('jenis_bot', 'count(*)', "where binary id = '$id_jenis_bot'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID Jenis Bot tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id_roles)) {
	$cekID = baca_tabel('roles', 'count(*)', "where binary id = '$id_roles'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID Roles tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (empty($id)) {
	$cekNama = baca_tabel('mega_prompt', 'count(*)', "where mega_prompt='$mega_prompt'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "Email $mega_prompt sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID('mega_prompt', 'id');
	$dataMegaPrompt['id'] = $id;
	$result = insert_tabel('mega_prompt', $dataMegaPrompt);
} else {
	$action = 'update';
	$result = update_tabel('mega_prompt', $dataMegaPrompt, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-mega-prompt', json_encode($dataMegaPrompt), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}
echo encryptData($datax);

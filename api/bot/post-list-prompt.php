<?php
include "../token/cek-token-admin.php";

// id_jenis_prompt, list_prompt
// list_prompt = id, id_mega_prompt, prompt

if (!empty($id_jenis_prompt)) {
	$cekID = baca_tabel('jenis_prompt', 'count(*)', "where binary id = '$id_jenis_prompt'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID Jenis Prompt tidak ada";
		echo encryptData($datax);
		die();
	}
}

foreach ($list_prompt as $prompts) {
	$id_list_prompt = $prompts['id'];
	$id_mega_prompt = $prompts['id_mega_prompt'];
	$prompt = $prompts['prompt'];
	if (empty($id_list_prompt)) {
		$cekID = baca_tabel('list_prompt', 'count(*)', "where binary id='$id_list_prompt' and binary id_jenis_prompt='$id_jenis_prompt'");

		if ($cekID > 0) {
			$datax['code'] = 404;
			$datax['msg'] = "Email $mega_prompt sudah tersedia";
			echo encryptData($datax);
			die();
		}
		$action = 'create';
		$id = generateID('list_prompt', 'id');
		$dataListPrompt['id'] = $id_list_prompt;
		$dataListPrompt['id_mega_prompt'] = $id_mega_prompt;
		$dataListPrompt['id_jenis_prompt'] = $id_jenis_prompt;
		$dataListPrompt['prompt'] = $prompt;
		$result = insert_tabel('list_prompt', $dataListPrompt);
	} else {
		$action = 'update';
		$dataListPrompt['prompt'] = $prompt;
		$result = update_tabel('list_prompt', $dataListPrompt, "where binary id='$id_list_prompt' and binary id_jenis_prompt='$id_jenis_prompt' and binary id_mega_prompt='$id_mega_prompt'");
	}
}

if ($result) {
	activity_user($id_user, 'post-list-prompt', json_encode($dataListPrompt), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}
echo encryptData($datax);

<?php
include "../token/cek-token.php";

// title, caption, price

$id_project = generateID('project', 'id');
$dataProject['id'] = $id_project;
$dataProject['id_user'] = $id_user;
$dataProject['title'] = $title;
$dataProject['caption'] = $caption;
$dataProject['harga'] = $price;
$dataProject['waktu'] = date("Y-m-d H:i:s");

$result = insert_tabel('project', $dataProject);

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil Mengupload Project';
	$datax['tabs'] = 'project';
	$datax['kode'] = $id_project;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Mengupload project";
}
echo encryptData($datax);

<?php
include "../token/cek-token.php";

// id_transaksi

// orders = [
//     {
//         "jumlah" : 1,
//         "produk" : "id_produk_berdasarkan_jenis",
//         "varian": "id_varian_kombinasi"
//     },
//     {
//         "jumlah" : 1,
//         "produk" : "id_produk_berdasarkan_jenis",
//         "varian": "id_varian_kombinasi"
//     }
// ];

foreach ($orders as $order) {
    $id_produk_berdasarkan_jenis = $order['produk'];
    $id_varian_kombinasi = $order['varian'];
    $jumlah = $order['jumlah'];

    $cekVarian = baca_tabel('varian_kombinasi a join produk_berdasarkan_jenis b on a.id_jenis_produk=b.id_jenis_produk', 'count(*)', "where a.id='$id_varian_kombinasi' and b.id='$id_produk_berdasarkan_jenis'");

    if ($cekVarian <= 0) {
        $datax['code'] = 500;
        $datax['msg'] = 'Terjadi Kesalahan, Silahkan Coba Kembali';
        echo encryptData($datax);
        die();
    }

    $sqlProduk = "SELECT a.nama as nama_produk, a.makna, c.nama as nama_jenis_produk, d.harga
    FROM produk a
    JOIN produk_berdasarkan_jenis b ON a.id = b.id_produk 
    JOIN jenis_produk c ON b.id_jenis_produk = c.id 
    LEFT JOIN varian_kombinasi d ON c.id = d.id_jenis_produk
    WHERE f.id = '$id_varian_kombinasi' AND b.id = '$id_produk_berdasarkan_jenis'";

    $runProduk = $db->Execute($sqlProduk);

    $produkdetail = [];

    while ($getProduk = $runProduk->fetchRow()) {
        $sqlVarian = "SELECT c.nama as nama_varian
        FROM varian_kombinasi_list a
        LEFT JOIN varian_kombinasi b ON a.id_varian_kombinasi = b.id
        LEFT JOIN varian_detail c ON a.id_varian_detail = c.id
        WHERE a.id_varian_kombinasi = '$id_varian_kombinasi";

        $runVarian = $db->Execute($sqlVarian);

        while ($getVarian = $runVarian->fetchRow()) {
            $listVarianDetail[] = $getVarian;
        }

        $getProduk['varian'] = implode(", ", $listVarianDetail);

        unset($listVarianDetail);

        $produkdetail = $getProduk;
    }

    $produkdetail['harga'] = round($produkdetail['harga']);
    $produkdetail['jumlah'] = $jumlah;

    $produk[] = $produkdetail;
}

if (!empty($id_alamat)) {
    $kondisiAlamat = "AND id = '$id_alamat'";
}

$sqlAlamat = "SELECT *
FROM alamat
WHERE binary id_user = '$id_user' AND is_main = 1 $kondisiAlamat";

$runAlamat = $db->Execute($sqlAlamat);

$alamat = [];

while ($getAlamat = $runAlamat->fetchRow()) {
    $alamat = $getAlamat;
}

$totalProduk = 0;

$hargaProduk += $produk['harga'];
$jumlahProduk += $produk['jumlah'];

$id_mitra = baca_tabel('mitra', 'id', "where id_user = '$id_user'");

if (!empty($id_mitra)) {
    $diskon  = 10 / 100;
    if ($jumlahProduk > 24) {
        $potongan = $hargaProduk * $diskon * -1;
    } else {
        $selisih = 24 - $jumlahProduk;
        $info = ["code" => 2, "msg" => "Tambah $selisih produk akan dapat diskon sebesar 10%"];
    }
} else {
    if ($jumlahProduk > 24) {
        $info = ["code" => 2, "msg" => "Daftar Mitra akan dapat diskon sebesar 10%"];
    } else {
        $info = ["code" => 2, "msg" => "Daftar Mitra akan dapat banyak benefit yang menguntungkan"];
    }
}

$harga['jumlah'] = $jumlahProduk;
$harga['harga'] = $hargaProduk;
$harga['potongan'] = $potongan;

$transaksi['info'] = $info;
$transaksi['produk'] = $produk;
$transaksi['alamat'] = $alamat;
$transaksi['harga'] = $harga;

$datax['code'] = 200;
$datax['transaksi'] = $transaksi;

echo encryptData($datax);
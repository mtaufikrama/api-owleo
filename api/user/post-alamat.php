<?php
include "../token/cek-token.php";

// id, alamat, id_provinsi, id_kota, id_kecamatan, id_kelurahan, is_main, lat, lang

if (!empty($id)) {
	$cekID = baca_tabel('alamat', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

$id_cart = generateID('alamat', 'id');

$dataCart['total'] = $total;
$dataCart['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$dataCart['id_user'] = $id_user;
	$dataCart['id_produk_berdasarkan_jenis'] = $id_produk_berdasarkan_jenis;
	$result = insert_tabel('cart', $dataCart);
} else {
	$dataCart['id'] = $id_cart;
	$result = update_tabel('cart', $dataCart, "where binary id='$id'");
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil Mengupload Cart';
	$datax['kode'] = $id_cart;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Mengupload Cart";
}

echo encryptData($datax);

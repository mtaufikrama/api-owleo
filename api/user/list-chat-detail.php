<?php
include "../token/cek-token.php";

// username, offset, limit

if (empty($limit)) $limit = 10;
if (empty($offset)) $offset = 0;

if (empty($username)) {
	$datax['code'] = 404;
	$datax['msg'] = "Tidak ada Username";
	echo encryptData($datax);
	die();
}

$cekUsername = baca_tabel('user', 'count(*)', "where username='$username'");

if ($cekUsername <= 0) {
	$datax['code'] = 404;
	$datax['msg'] = "Username tidak ditemukan";
	echo encryptData($datax);
	die();
}

$id_user_lawan = baca_tabel('user', 'id', "where username='$username'");

$sql = "SELECT id, id_user, pesan from live_chat
where (binary id_user='$id_user' and binary id_user_lawan='$id_user_lawan')
or (binary id_user='$id_user_lawan' and binary id_user_lawan='$id_user') 
order by waktu desc limit $limit offset $offset";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
	$get['user'] = get_user($get['id_user']);
	if ($get['id_user'] == $id_user) {
		$get['isMe'] = true;
	} else {
		$get['isMe'] = false;
	}
	unset($get['id_user']);
	$data[] = $get;
}

if (is_array($data)) {
	$datax['code'] = 200;
	$datax['chat_detail'] = $data;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Tidak ada data ditemukan";
}
echo encryptData($datax);

<?php
include "../token/cek-token.php";

// caption

if (!$caption || $caption == '') {
	$datax['code'] = 500;
	$datax['msg'] = "Title tidak ada";
	echo encryptData($datax);
	die();
}

$id_kritik_saran = generateID('kritik_saran', 'id');
$dataSkill['id'] = $id_kritik_saran;
$dataSkill['id_kritik_saran'] = $id_kritik_saran;
$dataSkill['id_user'] = $id_user;
$dataSkill['caption'] = $caption;
$dataSkill['waktu'] = date("Y-m-d H:i:s");

$result = insert_tabel('kritik_saran', $dataSkill);

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil Mengupload Kritik Saran';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Mengupload Kritik Saran";
}
echo encryptData($datax);

<?php
include "../token/cek-token.php";

// username, pesan

if (empty($username)) {
	$datax['code'] = 404;
	$datax['msg'] = "Tidak ada Username";
	echo encryptData($datax);
	die();
}

$cekUsername = baca_tabel('user', 'count(*)', "where username='$username'");

if ($cekUsername <= 0) {
	$datax['code'] = 404;
	$datax['msg'] = "Username tidak ditemukan";
	echo encryptData($datax);
	die();
}

$id_user_lawan = baca_tabel('user', 'id', "where username='$username'");

$id = generateID('live_chat', 'id');
$dataChat['id'] = $id;
$dataChat['id_user'] = $id_user;
$dataChat['id_user_lawan'] = $id_user_lawan;
$dataChat['pesan'] = $pesan;
$dataChat['waktu'] = date("Y-m-d H:i:s");

$result = insert_tabel('live_chat', $dataChat);

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Terkirim";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Tidak ada data ditemukan";
}
echo encryptData($datax);

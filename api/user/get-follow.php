<?php
include "../token/cek-no-token.php";

// username

if (empty($username)) {
    $datax['code'] = 404;
    $datax['msg'] = "Tidak ada Username";
    echo encryptData($datax);
    die();
}

$cekUsername = baca_tabel('user', 'count(*)', "where username='$username'");

if ($cekUsername <= 0) {
    $datax['code'] = 404;
    $datax['msg'] = "Username tidak ditemukan";
    echo encryptData($datax);
    die();
}

$id_user_lawan = baca_tabel('user', 'id', "where username='$username'");

$cekFollow = baca_tabel('followers', 'is_follow', "where binary id_user='$id_user' and binary id_user_lawan='$id_user_lawan'");

if ($cekFollow > 0) {
    $isFollow = true;
} else {
    $isFollow = false;
}

$datax['code'] = 200;
$datax['is_follow'] = $isFollow;

echo encryptData($datax);

<?php
include "../token/cek-no-token.php";

// id

// $db->debug = true;

$id_tabs = baca_tabel("feed", "id_tabs", "where binary id='$id' and action<>2");

if (!$id_tabs || $id_tabs == '') {
	$datax['code'] = 404;
	$datax['msg'] = "Data Tidak Ditemukan";
	echo encryptData($datax);
	die();
}

$sql = "SELECT a.id, a.kode, a.id_user,
	b.nama as tabs, a.caption, a.waktu
	FROM feed a 
	JOIN tabs b ON a.id_tabs=b.id 
	WHERE binary a.id='$id'
	and a.action<>2";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {

	$get['user'] = get_user($get['id_user']);
	unset($get['id_user']);

	$get['waktu'] = date2pesan($get['waktu']);

	$tabs = $get['tabs'];
	$kode = $get['kode'];

	$get['tabs'] = 'feed';

	$idTabsFeed = '1b2IDNZbMY5JJ0e';

	$sqlImg = "SELECT id_images as id FROM tabs_img 
		WHERE binary kode='$id' 
		and binary id_tabs='$idTabsFeed' 
		and action<>2";

	$runImg = $db->Execute($sqlImg);

	while ($getImg = $runImg->fetchRow()) {
		$idImg = $getImg['id'];
		$images[] = image_link($idImg);
	}

	$cekLike = baca_tabel('likes', 'count(*)', "where binary kode='$id' 
		and binary id_tabs='$idTabsFeed' 
		and is_like='1' 
		and id_user='$id_user'");

	if ($cekLike > 0) {
		$isLike = true;
	} else {
		$isLike = false;
	}

	$get['images'] = $images;
	unset($images);
	$get['is_like'] = $isLike;
	$get['jml_like'] = baca_tabel('likes', 'count(*)', "where binary kode='$id' 
		and binary id_tabs='$idTabsFeed' 
		and is_like='1'");
	$get['jml_comment'] = baca_tabel('comment', 'count(*)', "where binary kode='$id' 
		and binary id_tabs='$idTabsFeed'");

	if ($tabs != 'feed') {
		$idImg = baca_tabel("tabs_img", 'id_images', "where binary id_tabs='$id_tabs' 
		and binary kode='$kode' 
		and action<>2");
		$detail['id'] = $kode;
		$detail['tabs'] = $tabs;
		$detail['image'] = image_link($idImg);
	}

	unset($get['kode']);

	$get['detail'] = $detail;

	$data = $get;
}

if (is_array($data)) {
	$datax['code'] = 200;
	$datax['feed'] = $data;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Data Tidak Ditemukan";
}

echo encryptData($datax);

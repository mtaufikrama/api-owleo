<?php
include "../token/cek-token.php";

// orders, (id_alamat)

// orders = [
//     {
//         "jumlah" : 1,
//         "produk" : "id_produk_berdasarkan_jenis",
//         "varian": "id_varian_kombinasi"
//     },
//     {
//         "jumlah" : 1,
//         "produk" : "id_produk_berdasarkan_jenis",
//         "varian": "id_varian_kombinasi"
//     }
// ];

$totalBeratProduk = 0;
$hargaProduk  = 0;
$jumlahProduk  = 0;

foreach ($orders as $order) {
    $id_produk_berdasarkan_jenis = $order['produk'];
    $id_varian_kombinasi = $order['varian'];
    $jumlah = round($order['jumlah']);

    $cekVarian = baca_tabel('varian_kombinasi a join produk_berdasarkan_jenis b on a.id_jenis_produk=b.id_jenis_produk', 'count(*)', "where a.id='$id_varian_kombinasi' and b.id='$id_produk_berdasarkan_jenis'");

    if ($cekVarian <= 0) {
        $datax['code'] = 500;
        $datax['msg'] = 'Terjadi Kesalahan, Silahkan Coba Kembali';
        echo encryptData($datax);
        die();
    }

    $sqlProduk = "SELECT a.nama as nama_produk, a.makna, c.nama as nama_jenis_produk, d.harga, c.berat
    FROM produk a
    JOIN produk_berdasarkan_jenis b ON a.id = b.id_produk 
    JOIN jenis_produk c ON b.id_jenis_produk = c.id 
    LEFT JOIN varian_kombinasi d ON c.id = d.id_jenis_produk
    WHERE f.id = '$id_varian_kombinasi' AND b.id = '$id_produk_berdasarkan_jenis'";

    $runProduk = $db->Execute($sqlProduk);

    $produkdetail = [];

    while ($getProduk = $runProduk->fetchRow()) {
        $sqlVarian = "SELECT c.nama as nama_varian
        FROM varian_kombinasi_list a
        LEFT JOIN varian_kombinasi b ON a.id_varian_kombinasi = b.id
        LEFT JOIN varian_detail c ON a.id_varian_detail = c.id
        WHERE a.id_varian_kombinasi = '$id_varian_kombinasi";

        $runVarian = $db->Execute($sqlVarian);

        while ($getVarian = $runVarian->fetchRow()) {
            $listVarianDetail[] = $getVarian;
        }

        $getProduk['varian'] = implode(", ", $listVarianDetail);

        unset($listVarianDetail);

        $produkdetail = $getProduk;
    }

    $produkdetail['berat_satuan'] = round($produkdetail['berat']);
    $produkdetail['berat_total'] = round($produkdetail['berat']) * $jumlah;
    $produkdetail['harga_satuan'] = round($produkdetail['harga']);
    $produkdetail['harga_total'] = round($produkdetail['harga']) * $jumlah;
    $produkdetail['jumlah'] = $jumlah;

    $totalBeratProduk += $produkdetail['berat_total'];
    $hargaProduk += $produkdetail['harga_total'];
    $jumlahProduk += $produkdetail['jumlah'];

    $produk[] = $produkdetail;
}

if (!empty($id_alamat)) {
    $kondisiAlamat = "AND id = '$id_alamat'";
}

$sqlAlamat = "SELECT *
FROM alamat
WHERE binary id_user = '$id_user' AND is_main = 1 $kondisiAlamat";

$runAlamat = $db->Execute($sqlAlamat);

$alamat = [];

while ($getAlamat = $runAlamat->fetchRow()) {
    $alamat = $getAlamat;
}

$id_mitra = baca_tabel('mitra', 'id', "where id_user = '$id_user'");

if (!empty($id_mitra)) {
    $diskon  = 10 / 100;
    if ($jumlahProduk > 24) {
        $potongan_harga = $hargaProduk * $diskon * -1;
    } else {
        $selisih = 24 - $jumlahProduk;
        $info = ["code" => 2, "msg" => "Tambah $selisih produk akan dapat diskon sebesar 10%"];
    }
} else {
    if ($jumlahProduk > 24) {
        $info = ["code" => 2, "msg" => "Daftar Mitra akan dapat diskon sebesar 10%"];
    } else {
        $info = ["code" => 2, "msg" => "Daftar Mitra akan dapat banyak benefit yang menguntungkan"];
    }
}

$id_alamat_destination = $alamat['id'];

$id_kota_origin = 154;
$id_kota_destination = baca_tabel('alamat a join kota b on a.id_kota=b.id_kota', 'b.id_rajaongkir', "where a.id='$id_alamat_destination'");
$weight = $totalBeratProduk;

include '../function/api_rajaongkir.php';
$rajaongkir = new rajaongkir();

$list_ongkir = [];
$min_value = PHP_INT_MAX;
$list_courier = ['jne', 'pos', 'tiki'];
foreach ($list_courier as $courier) {
    $ongkir_courier = $rajaongkir->cost($id_kota_origin, $id_kota_destination, $weight, $courier);
    foreach ($ongkir_courier['rajaongkir']['results'] as $results) {
        foreach ($results['costs'] as $costs) {
            foreach ($costs['cost'] as $cost) {
                if ($cost['value'] < $min_value) {

                    $min_value = $cost['value'];

                    $dataOngkir['code'] = $results['code'];
                    $dataOngkir['name'] = $results['name'];
                    $dataOngkir['service'] = $costs['service'];
                    $dataOngkir['description'] = $costs['description'];
                    $dataOngkir['etd'] = $cost['etd'];
                    $dataOngkir['note'] = $cost['note'];
                    $diskon_ongkir = 30 / 100;
                    $value = $cost['value'] - ($diskon_ongkir * $cost['value']);
                    $harga_ongkir = floor($value / 1000) * 1000;
                    $dataOngkir['harga_ongkir'] = $harga_ongkir;
                    $dataOngkir['value'] = $cost['value'];
                }
            }
        }
    }
}

$diskon_potongan_ongkir = 0;
if ($jumlahProduk >= 12) {
    $diskon_potongan_ongkir = 10000;
} else if ($jumlahProduk >= 6) {
    $diskon_potongan_ongkir = 6000;
} else if ($jumlahProduk >= 2) {
    $diskon_potongan_ongkir = 3000;
}

$selisih_ongkir = $dataOngkir['harga_ongkir'] - $diskon_potongan_ongkir;

if ($selisih_ongkir < 0) {
    $potongan_ongkir = $dataOngkir['harga_ongkir'];
} else {
    $potongan_ongkir = $diskon_potongan_ongkir;
}

$harga['jumlah'] = $jumlahProduk;
$harga['harga'] = $hargaProduk;
$harga['potongan_harga'] = $potongan_harga;
$harga['potongan_ongkir'] = $potongan_ongkir;
$harga['ongkir'] = $dataOngkir['harga_ongkir'];

$id_log_transaksi = generateID('id', 'log_transaksi');
$transaksi['id'] = $id_log_transaksi;
$transaksi['info'] = $info;
$transaksi['produk'] = $produk;
$transaksi['alamat'] = $alamat;
$transaksi['ongkir'] = $dataOngkir;
$transaksi['harga'] = $harga;

$log_transaksi['id'] = $id_log_transaksi;
$log_transaksi['id_user'] = $id_user;
$log_transaksi['json_transaksi'] = $transaksi;
$log_transaksi['waktu'] = date('Y-m-d H:i:s');

$result = insert_tabel('log_transaksi', $log_transaksi);

if ($result) {
    $datax['code'] = 200;
    $datax['transaksi'] = $transaksi;
} else {
    $datax['code'] = 500;
    $datax['msg'] = 'Terjadi Kesalahan, Silahkan Coba Kembali';
}

echo encryptData($datax);
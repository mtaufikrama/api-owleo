<?php
include "../token/cek-token.php";

// offset, limit

if (empty($limit)) $limit = 10;
if (empty($offset)) $offset = 0;

// $sql = "SELECT id, id_user_lawan 
// from live_chat 
// group by id_user_lawan 
// where binary id_user='$id_user'
// order by waktu desc";

$gmt = baca_tabel('login', 'gmt', "where binary token='$authorized'");

// chatgpt
$sql = "SELECT lc1.pesan AS last_chat,
lc1.waktu AS last_time,
CASE WHEN lc1.id_user = '$id_user' THEN lc1.id_user_lawan
	 WHEN lc1.id_user_lawan = '$id_user' THEN lc1.id_user
END AS id_lawan
FROM live_chat lc1
LEFT JOIN live_chat lc2 ON 
(lc1.id_user = lc2.id_user AND lc1.id_user_lawan = lc2.id_user_lawan AND lc1.waktu < lc2.waktu)
OR
(lc1.id_user_lawan = lc2.id_user AND lc1.id_user = lc2.id_user_lawan AND lc1.waktu < lc2.waktu)
WHERE lc2.id_user IS NULL AND (lc1.id_user = '$id_user' OR lc1.id_user_lawan = '$id_user')
ORDER BY lc1.waktu DESC limit $limit offset $offset";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
	$get['user'] = get_user($get['id_lawan']);
	// $get['last_chat'] = baca_tabel('live_chat', 'balasan', "where id = '" . $get['id'] . "' order by waktu desc");
	// $get['last_time'] = baca_tabel('live_chat', 'waktu', "where id = '" . $get['id'] . "' order by waktu desc limit 1");
	unset($get['id_lawan']);
	$get['last_time'] = date('Y-m-d H:i:s', strtotime($get['last_time']) + ($gmt * 3600));
	$data[] = $get;
}

if (is_array($data)) {
	$datax['code'] = 200;
	$datax['chat'] = $data;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Tidak ada data ditemukan";
}

echo encryptData($datax);

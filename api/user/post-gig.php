<?php
include "../token/cek-token.php";

// title, caption, price

$id_gig = generateID('gig', 'id');
$dataGig['id'] = $id_gig;
$dataGig['id_user'] = $id_user;
$dataGig['title'] = $title;
$dataGig['caption'] = $caption;
$dataGig['harga'] = $price;
$dataGig['waktu'] = date("Y-m-d H:i:s");

$result = insert_tabel('gig', $dataGig);

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil Mengupload Gig';
	$datax['tabs'] = 'gig';
	$datax['kode'] = $id_gig;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Mengupload Gig";
}

echo encryptData($datax);

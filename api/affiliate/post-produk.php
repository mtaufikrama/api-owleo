<?php
include "../token/cek-token-affiliate.php";

// id, nama, olshop_product, id_category_product

// olshop_product = id, id_olshop, link

if (empty($nama)) {
	$datax['code'] = 500;
	$datax['msg'] = "Nama Produk tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('product_affiliate', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

$no_urut_terakhir = baca_tabel('product_affiliate', 'max(no_urut)', "where id_user='$id_user'");

$no_urut = round($no_urut_terakhir) + 1;

$dataProduct['id_user'] = $id_user;
$dataProduct['no_urut'] = $no_urut;
$dataProduct['id_category_product_affiliate'] = $id_category_product;
$dataProduct['nama'] = $nama;
$dataProduct['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$id = generateID('product_affiliate', 'id');
	$dataProduct['id'] = $id;
	$result = insert_tabel('product_affiliate', $dataProduct);
} else {
	$result = update_tabel('product_affiliate', $dataProduct, "where binary id='$id'");
}

if ($result) {
	foreach ($olshop_products as $link) {
		$id_olshop_product_affiliate = $link['id'];
		$linkProduk['id_product_affiliate'] = $id;
		$linkProduk['id_olshop'] = $link['olshop']['id'];
		$linkProduk['link'] = $link['link'];
		$linkProduk['waktu'] = date("Y-m-d H:i:s");

		if (!empty($linkProduk['link'])) {
			if (empty($id_olshop_product_affiliate)) {
				$linkProduk['id'] = generateID('olshop_product_affiliate', 'id');
				if ($result) $result = insert_tabel('olshop_product_affiliate', $linkProduk);
			} else {
				$cekID = baca_tabel('olshop_product_affiliate', 'count(*)', "where binary id = '$id_olshop_product_affiliate'");
				if ($cekID > 0) {
					if ($result) $result = update_tabel('olshop_product_affiliate', $linkProduk, "where binary id='$id_olshop_product_affiliate'");
				}
			}
		}
	}
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Produk";
	$datax['id'] = $id;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Produk";
}
echo encryptData($datax);

<?php
include "../token/cek-token-affiliate.php";

// id, nama

if (empty($nama)) {
	$datax['code'] = 500;
	$datax['msg'] = "Nama Kategori tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('category_product_affiliate', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

$dataKategori['id_user'] = $id_user;
$dataKategori['nama'] = $nama;
$dataKategori['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$id = generateID('category_product_affiliate', 'id');
	$dataKategori['id'] = $id;
	$result = insert_tabel('category_product_affiliate', $dataKategori);
} else {
	$result = update_tabel('category_product_affiliate', $dataKategori, "where binary id='$id'");
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Produk";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Produk";
}
echo encryptData($datax);

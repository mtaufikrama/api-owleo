<?php
include "../token/cek-no-token.php";

// id, username, query, id_kategori

if (empty($id_user)) {
	if (empty($username)) {
		$datax['code'] = 500;
		$datax['msg'] = "Username tidak ditemukan";
		echo encryptData($datax);
		die();
	} else {
		$id_userz = baca_tabel('affiliate', 'id_user', "where username='$username'");
	}
} else {
	$id_userz = $id_user;
}

if (!empty($query)) {
	if (is_numeric($query)) {
		$sqlPlus = "AND no_urut like '%$query%'";
	} else {
		$sqlPlus = "AND nama like '%$query%'";
	}
}

if (!empty($id)) {
	$sqlPlus .= " AND binary id = '$id'";
}

if (!empty($id_kategori)) {
	$sqlPlus .= " AND binary id_category_product_affiliate = '$id_kategori'";
}

$sql = "SELECT id, nama, id_category_product_affiliate as id_kategori, no_urut FROM product_affiliate where binary id_user='$id_userz' $sqlPlus order by no_urut asc";

$run = $db->Execute($sql);
while ($get = $run->fetchRow()) {
	$id_product_affiliate = $get['id'];

	$sql_olshop_product = "SELECT id, id_olshop, link FROM olshop_product_affiliate where binary id_product_affiliate='$id_product_affiliate' order by waktu asc";
	$run_olshop_product = $db->Execute($sql_olshop_product);
	while ($get_olshop_product = $run_olshop_product->fetchRow()) {
		$id_olshop = $get_olshop_product['id_olshop'];

		$sql_olshop = "SELECT * FROM olshop where binary id='$id_olshop' limit 1";
		$run_olshop = $db->Execute($sql_olshop);
		$olshop = [];
		while ($get_olshop = $run_olshop->fetchRow()) {
			$olshop = $get_olshop;
		}
		$get_olshop_product['olshop'] = $olshop;
		unset($get_olshop_product['id_olshop']);
		$olshop_product[] = $get_olshop_product;
	}
	// $sql_konten = "SELECT id, id_images as path_file, waktu FROM content_product_affiliate where binary id_product_affiliate='$id_product_affiliate' order by waktu desc";
	// $run_konten = $db->Execute($sql_konten);
	// while ($get_konten = $run_konten->fetchRow()) {
	// 	$get_konten['waktu'] = date2pesan($get_konten['waktu']);
	// 	$konten[] = $get_konten;
	// }
	$id_kategori = $get['id_kategori'];
	$sql_kategori = "SELECT * FROM category_product_affiliate where binary id='$id_kategori' limit 1";
	$run_kategori = $db->Execute($sql_kategori);
	while ($get_kategori = $run_kategori->fetchRow()) {
		$kategori = $get_kategori;
	}
	$sql_foto = "SELECT id, id_images FROM product_affiliate_img where binary id_product_affiliate='$id_product_affiliate' order by no_urut asc";
	$run_foto = $db->Execute($sql_foto);
	while ($get_foto = $run_foto->fetchRow()) {
		$get_foto['path_file'] = image_link($get_foto['id_images']);
		unset($get_foto['id_images']);
		$foto[] = $get_foto;
	}
	$get['foto'] = $foto;
	unset($foto);
	// $get['konten'] = $konten;
	// unset($konten);
	$get['kategori'] = $kategori;
	unset($kategori);
	unset($get['id_kategori']);
	$get['olshop_product'] = $olshop_product;
	unset($olshop_product);
	$get['no_urut'] = round($get['no_urut']);
	$isEdit = false;
	if ($id_userz == $id_user) $isEdit = true;
	$get['is_edit'] = $isEdit;
	$products[] = $get;
}

if (is_array($products)) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
	$datax['products'] = $products;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Data Tidak Ditemukan";
}
echo encryptData($datax);

<?php

include "../token/cek-token.php";

// username, nama, foto

if (empty($username)) {
    $datax['code'] = 500;
    $datax['msg'] = "Username Harus Diisi";
    echo encryptData($datax);
    die();
}

$username = strtolower($username);

$cekSebelummasukusername = baca_tabel("affiliate", "count(username)", "where username='$username'");

if ($cekSebelummasukusername > 0) {
    $datax['code'] = 500;
    $datax['msg'] = "Username Sudah Terdaftar";
    echo encryptData($datax);
    die();
}

$dataRegist['id'] = generateID('affiliate', 'id');
$dataRegist['available'] = 1;
$dataRegist['username'] = $username;
$dataRegist['nama'] = $nama;
$dataRegist['waktu'] = date('Y-m-d H:i:s');

$result = insert_tabel("affiliate", $dataRegist);

if ($result) {
    $datax['code'] = 200;
    $datax['msg'] = $email;
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Maaf, Pendaftaran Gagal diproses";
}

echo encryptData($datax);

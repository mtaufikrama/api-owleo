<?php
include "../token/cek-token-affiliate.php";

// id, id_product_affiliate, file, no_urut

if (empty($id_product_affiliate)) {
	$datax['code'] = 500;
	$datax['msg'] = "ID Produk Produk tidak ada";
	echo encryptData($datax);
	die();
}

if (empty($file)) {
	if (!is_array($_FILES)) {
		$datax['code'] = 500;
		$datax['msg'] = "Foto tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id)) {
	$cekID = baca_tabel('product_affiliate', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($file)) {
	$parsed_url = parse_url($file);
	$local_path = ltrim($parsed_url['path'], '/');
	$fileName = $local_path;
	$date = date("Y-m-d H:i:s");
	$result = true;
} else {
	$file = $_FILES['file'];

	if (!is_array($file)) {
		$datax['code'] = 500;
		$datax['msg'] = 'Tidak ada konten yang diinput';
		echo encryptData($datax);
		die();
	}

	if ($file['size'] >= 200000000) {
		$datax['code'] = 500;
		$datax['msg'] = 'Data Terlalu Besar';
		echo encryptData($datax);
		die();
	}

	$fileName = basename($file['name']);
	$date = date("Y-m-d H:i:s");
	$id_affiliate = baca_tabel('affiliate', 'id', "where binary id_user='$id_user'");
	$pathDir = "assets/affiliate/$id_affiliate/product/";
	$homeDir = __DIR__ . "/../../" . $pathDir;
	if (!is_dir($homeDir)) {
		$cekMkDir = mkdir($homeDir, 0777, true);
		if (!$cekMkDir) {
			$datax['code'] = 500;
			$datax['msg'] = "Tidak Berhasil Menambahkan Folder $pathDir";
			echo encryptData($datax);
			die();
		}
	}

	$result = move_uploaded_file($file['tmp_name'], $homeDir . $fileName);

	$fileName = $pathDir . $fileName;
}

$dataContent['id_user'] = $id_user;
$dataContent['id_product_affiliate'] = $id_product_affiliate;
$dataContent['path_product'] = $fileName;
$dataContent['waktu'] = $date;

if (empty($id)) {
	$id = generateID('product_affiliate_img', 'id');
	$dataContent['id'] = $id;
	if ($result) $result = insert_tabel('product_affiliate_img', $dataContent);
} else {
	if ($result) $result = update_tabel('product_affiliate_img', $dataContent, "where binary id='$id'");
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Konten Produk";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Konten Produk";
}

echo encryptData($datax);

<?php
if (!function_exists("image_link")) {
    function image_link($id_images = '')
    {
        if (empty($id_images)) {
            $link = null;
        } else {
            $link = "https://api-owleo.metir.my.id/api/image?id=$id_images";
        }
        return $link;
    }
}

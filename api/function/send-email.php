<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include "../../library/PHPMailer.php";
include "../../library/Exception.php";
include "../../library/OAuth.php";
include "../../library/POP3.php";
include "../../library/SMTP.php";

if (!function_exists("send_email")) {
    function send_email($send_email_name = "", $email = "", $params = array())
    {

        global $db;

        $sql = "SELECT nama, subject, body, id_email from send_email where binary send_email_name = '$send_email_name'";
        $run = $db->Execute($sql);
        while ($get = $run->fetchRow()) {
            $sqlEmail = "SELECT email, password from email where binary id = '" . $get['id_email'] . "'";
            $runEmail = $db->Execute($sqlEmail);
            while ($getEmail = $runEmail->fetchRow()) {
                $data = $getEmail;
            }
            $data += $get;
        }

        if (!is_array($data)) {
            return false;
        }

        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = "mail.metlance.com"; //host mail server (sesuaikan dengan mail hosting Anda)
        $mail->SMTPAuth = true;

        $mail->Username = $data['email']; //nama-email smtp
        $mail->Password = dekrip(base64_decode($data['password'])); //password email smtp

        $mail->SMTPSecure = "ssl";
        $mail->Port = 465;
        $mail->From = $data['email']; //email pengirim
        $mail->FromName = $data['nama']; //nama pengirim
        $mail->addAddress($email, "");

        $recipients = array(
            $data['email'] => 'CC 1',
            // ..
        ); /**/
        foreach ($recipients as $email => $name) {
            $mail->AddCC($email, $name);
        }

        $mail->isHTML(true);
        $subject = $data['subject'];
        $message = $data['body'];
        foreach ($params as $key => $value) {
            $message = str_replace("{{" . $key . "}}", $value, $message);
        }
        $mail->Subject = $subject;
        $mail->Body = nl2br($message);
        $mail->AltBody = "PHP mailer";
        $result = $mail->send();
        return $result;
    }
}

if (!function_exists("device_name")) {
    function device_name($device = array())
    {
        return "Chrome";
    }
}

if (!function_exists("location_name")) {
    function location_name($location = array())
    {
        return "Jakarta";
    }
}

if (!function_exists("location_for_email")) {
    function location_for_email($token = "")
    {
        $device_data = baca_tabel('login', 'device_data', "where binary token = '$token'");
        $gmt = baca_tabel('login', 'gmt', "where binary token = '$token'");
        $params['device_name'] = device_name($device_data);
        $gmtName = " GMT+$gmt";
        if ($gmt < 0) $gmtName = " GMT$gmt";
        $params['waktu_perubahan'] = date_time(date("Y-m-d H:i:s"), $token) . $gmtName;
        $params['lokasi'] = location_name();
        return $params;
    }
}

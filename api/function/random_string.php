<?php

if (!function_exists("randomString")) {
    function randomString($length = 10, $isRandom = false, $onlyNumber = false)
    {
        if ($isRandom) $length = rand(5, $length);
        if ($onlyNumber) $characters = '0123456789';
        else $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        $charLength = strlen($characters);

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charLength - 1)];
        }

        return $randomString;
    }
}

if (!function_exists("randomOtp")) {
    function randomOtp()
    {
        $randomString = randomString(6, false, true);

        $ulangi = 0;

        while ($ulangi == 0) {
            $cek = baca_tabel('otp', 'otp', "where binary otp = '$randomString'");
            if (!empty($cek)) {
                $randomString = randomString(6, false, true);
            } else {
                $ulangi = 1;
            }
        }

        return $randomString;
    }
}

if (!function_exists("generateID")) {
    function generateID($tbl = '', $fld = '', $minLength = 5)
    {
        global $db;

        $sql = "SHOW COLUMNS FROM $tbl LIKE '$fld'";

        $run = $db->Execute($sql);

        $row = [];
        while ($get = $run->fetchRow()) {
            $row = $get;
        }
        preg_match('/\((.*?)\)/', $row['Type'], $matches);
        $length = round($matches[1]);

        $ulangi = 0;
        $generateString = randomString($minLength);

        while ($ulangi == 0) {
            $cek = baca_tabel($tbl, $fld, "where binary $fld = '$generateString'");
            if ($cek) {
                if (strlen($generateString) >= $length) {
                    $generateString = randomString(5);
                } else {
                    $generateString .= randomString(1);
                }
            } else {
                $ulangi = 1;
            }
        }
        return $generateString;
    }
}

if (!function_exists("generateToken")) {
    function generateToken()
    {
        $ulangi = 0;
        $generateString = randomString(200, true);

        while ($ulangi == 0) {
            $cek = baca_tabel('login', 'token', "where binary token = '$generateString'");
            if ($cek) {
                $generateString = randomString(200, true);
            } else {
                $ulangi = 1;
            }
        }
        return $generateString;
    }
}

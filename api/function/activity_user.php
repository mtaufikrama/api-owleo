<?php
if (!function_exists("activity_user")) {
    function activity_user($id_userz = '', $title = '', $content = '', $action = '')
    {
        $data['id'] = generateID('activity', 'id');
        $data['id_user'] = $id_userz;
        $data['title'] = $title;
        $data['content'] = $content;
        $data['aksi'] = $action;
        $data['waktu'] = date('Y-m-d H:i:s');
        $result = insert_tabel('activity', $data);
        return $result;
    }
}

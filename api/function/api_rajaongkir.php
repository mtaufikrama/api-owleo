<?php

class rajaongkir
{

    public $debug;


    function __construct()
    {
    }

    function decompress($string)
    {

        return \LZCompressor\LZString::decompressFromEncodedURIComponent($string);
    }

    function loop($test)
    {
        foreach ($test as $key => $val) {
            if (is_array($val)) {
                foreach ($val as $key1 => $val1) {
                    if (is_array($val1)) {
                        // --- array --- 2 ---
                        foreach ($val1 as $key2 => $val2) {
                            if (is_array($val2)) {
                                // --- array --- 3 ---
                                foreach ($val2 as $key3 => $val3) {
                                    if (is_array($val3)) {
                                        // --- array --- 4 ---
                                        foreach ($val3 as $key4 => $val4) {
                                            if (is_array($val4)) {
                                                // --- array --- 5 ---
                                                foreach ($val4 as $key5 => $val5) {
                                                    if (is_array($val5)) {
                                                        // --- array --- 6 ---
                                                        foreach ($val5 as $key6 => $val6) {
                                                            if (is_array($val6)) {
                                                                // --- array --- 7 ---
                                                                foreach ($val6 as $key7 => $val7) {
                                                                    if (is_array($val7)) {
                                                                        // --- array --- 8 ---
                                                                        foreach ($val7 as $key8 => $val8) {
                                                                            if (is_array($val8)) {
                                                                                // --- array --- 9 ---

                                                                                // --- array --- 9 ---
                                                                            } else {
                                                                                echo "$key.$key1.$key2.$key3.$key4.$key5.$key6.$key7.$key8: $val8 <br>";
                                                                            }
                                                                        }
                                                                        // --- array --- 8 ---
                                                                    } else {
                                                                        echo "$key.$key1.$key2.$key3.$key4.$key5.$key6.$key7: $val7 <br>";
                                                                    }
                                                                }
                                                                // --- array --- 7 ---
                                                            } else {
                                                                echo "$key.$key1.$key2.$key3.$key4.$key5.$key6: $val6 <br>";
                                                            }
                                                        }
                                                        // --- array --- 6 ---
                                                    } else {
                                                        echo "$key.$key1.$key2.$key3.$key4.$key5: $val5 <br>";
                                                    }
                                                }
                                                // --- array --- 5 ---
                                            } else {
                                                echo "$key.$key1.$key2.$key3.$key4: $val4 <br>";
                                            }
                                        }
                                        // --- array --- 4 ---
                                    } else {
                                        echo "$key.$key1.$key2.$key3: $val3 <br>";
                                    }
                                }
                                // --- array --- 3 ---
                            } else {
                                echo "$key.$key1.$key2: $val2 <br>";
                            }
                        }
                        // --- array --- 2 ---
                    } else {
                        echo "$key.$key1: $val1 <br>";
                    }
                }
            } else {
                echo "$key: $val <br>";
            }
        }
    }
    // GET
    function SendRequest($path = '', $body = array(), $method = 'GET', $headers = array(), $query_params = array())
    {
        global $db;
        $url = "https://api.rajaongkir.com/starter" . $path;
        // Membuat URL dengan query parameter
        if ($query_params != array()) $url .= '?' . http_build_query($query_params);

        if ($this->debug) {
            echo json_encode($headers) . "<br>";
            echo "<pre>";
            print_r(json_encode($body));
            echo "</pre>";
            echo "url:" . $url . "<br>";
        }

        $body = http_build_query($body);

        // Inisialisasi cURL
        $ch = curl_init($url);

        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if ($body != []) curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        if ($headers != []) curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Mengatur opsi cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "$method");

        // Melakukan HTTP POST request
        $response = curl_exec($ch);

        if ($response != false || $response != null) {
            $response = json_decode($response, true);
        }

        // Menutup koneksi cURL
        curl_close($ch);
        //------------------------- Simpan Request --------------------------------
        if ($this->debug) {
            $db->debug = true;
        }
        // $db->BeginTrans();
        // $dataReq['url']        = $url;
        // $dataReq['method']    = $method;
        // $dataReq['payload']    = $body;
        // $dataReq['time_req']    = date("Y-m-d H:i:s");
        // $dataReq['result']    = json_encode($response);
        // $dataReq['header']    = json_encode($headers);
        // insert_tabel("log_ss_api_post", $dataReq);
        // $db->CommitTrans();
        //------------------------- Send Request --------------------------------
        if ($this->debug) {
            $this->loop($response);
            // echo "Tidak dapat menyambung ke server";
        }

        return $response;
    }


    function cost($id_kota_origin = '', $id_kota_destination = '', $weight = '', $courier = '')
    {
        $path = "/cost";

        // $query_params = [
        //     "grant_type" => "client_credentials",
        // ];

        $body = [
            "origin" => $id_kota_origin,
            "destination" => $id_kota_destination,
            "weight" => $weight,
            "courier" => $courier,
        ];

        $headers = ["Content-type: application/x-www-form-urlencoded", "key: 909f707db0ecc19c5176b8a583d78e0b"];

        $response = $this->SendRequest($path, $body, "POST", $headers);

        return $response;
    }

    function kota($id_kota = '', $id_provinsi = '')
    {
        $path = "/city";

        $query_params = [
            "id" => $id_kota,
            "province" => $id_provinsi,
        ];

        $body = [];

        $headers = ["key: 909f707db0ecc19c5176b8a583d78e0b"];

        $response = $this->SendRequest($path, $body, "GET", $headers);

        return $response;
    }

    function provinsi($id_provinsi = '')
    {
        $path = "/province";

        $query_params = [
            "id" => $id_provinsi,
        ];

        $body = [];

        $headers = ["key: 909f707db0ecc19c5176b8a583d78e0b"];

        $response = $this->SendRequest($path, $body, "GET", $headers);

        return $response;
    }
}
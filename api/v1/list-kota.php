<?php
include "../token/cek-token.php";

//id_provinsi 

if (empty($id_provinsi)) {
    $data['code'] = 404;
    $data['msg'] = 'ID Provinsi tidak ada';
    echo encryptData($data);
    die;
}

$sql = "SELECT ID_KOTA as kode,NAMA_KOTA as nama from kota where ID_PROVINSI='$id_provinsi'";

$run = $db->Execute($sql);
while ($get = $run->fetchRow()) {
    $kota[] = $get;
}

if (is_array($kota)) {
    $data['code'] = 200;
    $data['lists'] = $kota;
} else {
    $data['code'] = 500;
    $data['msg'] = "data tidak ditemukan";
}
echo encryptData($data);

<?php

include '../token/cek-no-token.php';

// jumlah, varian_kombinasi

// $varian_kombinasi = ["id_varian_detail","id_varian_detail"];

if (empty($jumlah)) {
    $datax['code'] = 404;
    $datax['msg'] = "Jumlah tidak ada";
    echo json_encode($datax);
    die();
}

if (empty($varian_kombinasi)) {
    $datax['code'] = 404;
    $datax['msg'] = "Varian tidak ada";
    echo json_encode($datax);
    die();
}

$varian_detail_list = implode(',', array_map(function ($item) {
    return "'$item'";
}, $varian_kombinasi));

$sql = "SELECT DISTINCT b.id, b.harga
    FROM varian_kombinasi_list a
    JOIN varian_kombinasi b ON a.id_varian_kombinasi = b.id
    WHERE a.id_varian_kombinasi IN (
        SELECT a.id_varian_kombinasi
        FROM varian_kombinasi_list a
        WHERE a.id_varian_detail IN ($varian_detail_list)
        GROUP BY a.id_varian_kombinasi
        HAVING COUNT(DISTINCT a.id_varian_detail) = " . count($varian_kombinasi) . "
    )
";

$cek = baca_tabel("($sql) as varian", "count(*)");

if ($cek != 1) {
    $datax['code'] = 404;
    $datax['msg'] = 'Tidak Ditemukan Harga';
    echo encryptData($datax);
    die();
}

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
    $$harga = $get['harga'];
}

$total = round($jumlah) * round($harga);

$datax['code'] = 200;
$datax['total'] = $total;

echo encryptData($datax);

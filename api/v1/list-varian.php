<?php
include "../token/cek-no-token.php";

//id_jenis_produk

if (empty($id_jenis_produk)) {
    $datax['code'] = 404;
    $datax['msg'] = "Jenis Produk Tidak Ditemukan";
    echo encryptData($datax);
    die();
}

$cekJenisProduk = baca_tabel("jenis_produk", "count(*)", "where id='$id_jenis_produk'");

if ($cekJenisProduk <= 0) {
    $datax['code'] = 404;
    $datax['msg'] = "Jenis Produk Tidak Ditemukan";
    echo encryptData($datax);
    die();
}

$sql = "SELECT id, nama as nama_varian from varian where id_jenis_produk='$id_jenis_produk'";

$run = $db->Execute($sql);
while ($get = $run->fetchRow()) {

    $id_varian = $get['id'];

    $sql = "SELECT id, nama as nama_varian_detail from varian_detail where id_varian='$id_varian'";
    $run = $db->Execute($sql);
    while ($get = $run->fetchRow()) {
        $varian_detail[] = $get;
    }
    $get['varian_detail'] = $varian_detail;
    $varian[] = $get;
    unset($get['varian_detail']);
}

if (is_array($varian)) {
    $datax['code'] = 200;
    $datax['varian'] = $kota;
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Varian tidak ditemukan";
}
echo encryptData($datax);

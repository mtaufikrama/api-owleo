<?php
include "../token/cek-token-admin.php";

// id, id_produk, id_jenis_produk

if (empty($id_produk)) {
	$datax['code'] = 500;
	$datax['msg'] = "ID Produk tidak ada";
	echo encryptData($datax);
	die();
}

if (empty($id_jenis_produk)) {
	$datax['code'] = 500;
	$datax['msg'] = "ID Jenis Produk tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('produk_by_jenis_produk', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id_produk)) {
	unset($cekID);
	$cekID = baca_tabel('produk', 'count(*)', "where binary id = '$id_produk'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID Produk tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id_jenis_produk)) {
	unset($cekID);
	$cekID = baca_tabel('jenis_produk', 'count(*)', "where binary id = '$id_jenis_produk'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID Jenis Produk tidak ada";
		echo encryptData($datax);
		die();
	}
}

$dataProdukByJenisProduk['id_produk'] = $id_produk;
$dataProdukByJenisProduk['id_jenis_produk'] = $id_jenis_produk;
$dataProdukByJenisProduk['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$id = generateID('produk_by_jenis_produk', 'id');
	$dataProdukByJenisProduk['id'] = $id;
	if ($result) $result = insert_tabel('produk_by_jenis_produk', $dataProdukByJenisProduk);
} else {
	if ($result) $result = update_tabel('produk_by_jenis_produk', $dataProdukByJenisProduk, "where binary id='$id'");
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Konten Produk";
	$datax['id'] = $id;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Konten Produk";
}

echo encryptData($datax);

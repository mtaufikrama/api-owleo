<?php
include "../token/cek-token-admin.php";

// id, nama, makna

if (empty($nama)) {
	$datax['code'] = 500;
	$datax['msg'] = "Nama Produk tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('produk', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

$dataProduk['id_user'] = $id_user;
$dataProduk['nama'] = $nama;
$dataProduk['makna'] = $makna;
$dataProduk['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$id = generateID('produk', 'id');
	$dataProduk['id'] = $id;
	$result = insert_tabel('produk', $dataProduk);
} else {
	$result = update_tabel('produk', $dataProduk, "where binary id='$id'");
}

if ($result) {
	foreach ($jenis_produk as $jenis) {
		$id_jenis_produk_desain_sobat_desain = $jenis['id'];
		$jenisProdukDesainSobatDesain['id_desain_sobat_desain'] = $id;
		$jenisProdukDesainSobatDesain['id_jenis_produk'] = $jenis['id_jenis_produk'];
		$jenisProdukDesainSobatDesain['waktu'] = date("Y-m-d H:i:s");

		if (empty($jenisProdukDesainSobatDesain['link'])) {
			if (empty($id_jenis_produk_desain_sobat_desain)) {
				$jenisProdukDesainSobatDesain['id'] = generateID('jenis_produk_desain_sobat_desain', 'id');
				if ($result) $result = insert_tabel('jenis_produk_desain_sobat_desain', $jenisProdukDesainSobatDesain);
			} else {
				$cekID = baca_tabel('jenis_produk_desain_sobat_desain', 'count(*)', "where binary id = '$id'");
				if ($cekID > 0) {
					if ($result) $result = update_tabel('jenis_produk_desain_sobat_desain', $jenisProdukDesainSobatDesain, "where binary id='$id_jenis_produk_desain_sobat_desain'");
				}
			}
		}
	}
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Produk";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Produk";
}
echo encryptData($datax);

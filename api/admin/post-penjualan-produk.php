<?php
include "../token/cek-token-admin.php";

// id_produk, list_penjualan

$list_penjualan = [
	[
		"id_produk_berdasarkan_jenis" => "id_produk_berdasarkan_jenis",
		"penjualan_olshop" => [
			[
				"id_olshop" => "id_olshop",
				"total" => 12
			]
		]
	],
	[
		"id_produk_berdasarkan_jenis" => "id_produk_berdasarkan_jenis",
		"penjualan_olshop" => [
			[
				"id_olshop" => "id_olshop",
				"total" => 12
			]
		]
	],
];

$hariIni = date('Y-m-d');

foreach ($list_penjualan as $penjualan) {
	$id_produk_berdasarkan_jenis = $penjualan['id_produk_berdasarkan_jenis'];
	$penjualan_olshop = $penjualan['penjualan_olshop'];
	foreach ($penjualan_olshop as $olshop) {

		$id_olshop = $olshop['id_olshop'];

		$dataPenjualanOlshop['total'] = $olshop['total'];
		$dataPenjualanOlshop['waktu'] = date("Y-m-d H:i:s");

		$cekWaktu = baca_tabel(
			'penjualan_produk_olshop',
			'count(*)',
			"where binary id_produk_berdasarkan_jenis='$id_produk_berdasarkan_jenis'
			and binary id_olshop='$id_olshop'
			and waktu BETWEEN '$hariIni 00:00:00' AND '$hariIni 23:59:59'"
		);

		if ($cekWaktu > 0) {
			$action = 'update';
			$result = update_tabel(
				'penjualan_produk_olshop',
				$dataPenjualanOlshop,
				"where binary id_produk_berdasarkan_jenis='$id_produk_berdasarkan_jenis'
				and binary id_olshop='$id_olshop'
				and waktu BETWEEN '$hariIni 00:00:00' AND '$hariIni 23:59:59'"
			);
		} else {
			$action = 'insert';
			$id = generateID('penjualan_produk_olshop', 'id');
			$dataPenjualanOlshop['id'] = $id;
			$dataPenjualanOlshop['id_produk_berdasarkan_jenis'] = $id_produk_berdasarkan_jenis;
			$dataPenjualanOlshop['id_olshop'] = $id_olshop;
			$result = insert_tabel('feed', $dataPenjualanOlshop);
		}
	}
}

if ($result) {
	activity_user($id_user, 'post-penjualan-produk', json_encode($dataPenjualanOlshop), $action);
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil $action Feed";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal $action Feed";
}
echo encryptData($datax);

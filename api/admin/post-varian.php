<?php
include "../token/cek-token-admin.php";

// id, id_jenis_produk, nama

if (!empty($id_jenis_produk)) $varian['id_jenis_produk'] = $id_jenis_produk;
if (!empty($nama)) $varian['nama'] = $nama;

if (!empty($id)) {
	$cekID = baca_tabel('varian', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (empty($id)) {
	$cekNama = baca_tabel('varian', 'count(*)', "where nama='$nama'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "Varian $nama sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID('varian', 'id');
	$varian['id'] = $id;
	$result = insert_tabel('varian', $varian);
} else {
	$action = 'update';
	$result = update_tabel('varian', $varian, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-varian', json_encode($varian), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}
echo encryptData($datax);

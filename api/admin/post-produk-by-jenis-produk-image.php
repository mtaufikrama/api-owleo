<?php
include "../token/cek-token-admin.php";

// id, id_produk_by_jenis_produk, file, no_urut

if (empty($id_produk_by_jenis_produk)) {
	$datax['code'] = 500;
	$datax['msg'] = "ID Jenis Produk tidak ada";
	echo encryptData($datax);
	die();
}

if (empty($file)) {
	if (!is_array($_FILES)) {
		$datax['code'] = 500;
		$datax['msg'] = "Foto tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id)) {
	$cekID = baca_tabel('produk_by_jenis_produk_img', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id_produk_by_jenis_produk)) {
	unset($cekID);
	$cekID = baca_tabel('produk_by_jenis_produk', 'count(*)', "where binary id = '$id_produk_by_jenis_produk'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($file)) {
	$parsed_url = parse_url($file);
	$local_path = ltrim($parsed_url['path'], '/');
	$fileName = $local_path;
	$date = date("Y-m-d H:i:s");
	$result = true;
} else {
	$file = $_FILES['file'];

	if (!is_array($file)) {
		$datax['code'] = 500;
		$datax['msg'] = 'Tidak ada konten yang diinput';
		echo encryptData($datax);
		die();
	}

	if ($file['size'] >= 200000000) {
		$datax['code'] = 500;
		$datax['msg'] = 'Data Terlalu Besar';
		echo encryptData($datax);
		die();
	}

	$fileName = $file['name'];
	$date = date("Y-m-d H:i:s");
	$id_produk = baca_tabel('produk_by_jenis_produk', 'id_produk', "where binary id = '$id_produk_by_jenis_produk'");
	$pathDir = "assets/product/$id_produk/";
	$homeDir = __DIR__ . "/../../" . $pathDir;
	if (!is_dir($homeDir)) {
		$cekMkDir = mkdir($homeDir, 0777, true);
		if (!$cekMkDir) {
			$datax['code'] = 500;
			$datax['msg'] = "Tidak Berhasil Menambahkan Folder $pathDir";
			echo encryptData($datax);
			die();
		}
	}

	$result = move_uploaded_file($file['tmp_name'], $homeDir . $fileName);

	$fileName = $pathDir . $fileName;
}

$dataProdukByJenisProdukImage['id_produk_by_jenis_produk'] = $id_produk_by_jenis_produk;
$dataProdukByJenisProdukImage['path_image'] = $fileName;
$dataProdukByJenisProdukImage['waktu'] = $date;

if (empty($id)) {
	$id = generateID('produk_by_jenis_produk_img', 'id');
	$dataProdukByJenisProdukImage['id'] = $id;
	if ($result) $result = insert_tabel('produk_by_jenis_produk_img', $dataProdukByJenisProdukImage);
} else {
	if ($result) $result = update_tabel('produk_by_jenis_produk_img', $dataProdukByJenisProdukImage, "where binary id='$id'");
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Foto Produk";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Foto Produk";
}

echo encryptData($datax);

<?php
include "../token/cek-token-admin.php";

// id

if (empty($id)) {
	$datax['code'] = 500;
	$datax['msg'] = "ID Tidak Ada";
	echo encryptData($datax);
	die();
}

$id_tabs = baca_tabel("feed", "id_tabs", "where binary id='$id' and action<>2");

if (!$id_tabs || $id_tabs == '') {
	$datax['code'] = 404;
	$datax['msg'] = "Data Tidak Ditemukan";
	echo encryptData($datax);
	die();
}

$sql = "SELECT a.id, c.id_user_img, a.kode, b.nama as tabs, 
a.caption, a.waktu, c.nama
FROM feed a 
JOIN tabs b ON a.id_tabs=b.id 
JOIN user c ON a.id_user=c.id 
WHERE binary a.id='$id' and action<>2";

// $sql = "SELECT a.id, c.id_user_img, a.kode, b.nama as tabs, 
// a.caption, a.waktu, c.nama, count(d.kode) as jml_like,
// FROM feed a 
// JOIN tabs b ON a.id_tabs=b.id 
// JOIN user c ON a.id_user=c.id 
// LEFT JOIN likes d ON a.id=d.kode
// LEFT JOIN comment e ON a.id=e.kode
// WHERE binary a.id='$id' 
// and binary d.id_tabs='$id_tabs' 
// and binary e.id_tabs='$id_tabs'
// GROUP BY a.id, c.id_user_img, a.kode, b.nama, 
// a.caption, a.waktu, c.nama
// ORDER BY jml_like DESC, a.waktu";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
	$idfoto = baca_tabel('user_img', 'id_images', "where binary id='" . $get['id_user_img'] . "'");
	$get['foto'] = image_link($idfoto);
	unset($get['id_user_img']);

	$get['waktu'] = date2pesan($get['waktu']);

	$tabs = $get['tabs'];
	$kode = $get['kode'];

	$get['tabs'] = 'feed';

	$idTabsFeed = '1b2IDNZbMY5JJ0e';

	$sqlImg = "SELECT id_images as id FROM tabs_img WHERE binary kode='$id' and binary id_tabs='$idTabsFeed' and action<>2";

	$runImg = $db->Execute($sqlImg);

	while ($getImg = $runImg->fetchRow()) {
		$idImg = $getImg['id'];
		$images[] = image_link($idImg);
	}

	$get['images'] = $images;
	unset($images);

	$get['jml_like'] = baca_tabel('likes', 'count(*)', "where binary kode='$id' and binary id_tabs='$idTabsFeed'");
	$get['jml_comment'] = baca_tabel('comment', 'count(*)', "where binary kode='$id' and binary id_tabs='$idTabsFeed'");

	if ($tabs != 'feed') {
		$idImg = baca_tabel("tabs_img", 'id_images', "where binary id_tabs='$id_tabs' and binary kode='$kode' and action<>2");
		$detail['id'] = $kode;
		$detail['tabs'] = $tabs;
		$detail['image'] = image_link($idImg);
	}

	unset($get['kode']);

	$get['detail'] = $detail;

	$data = $get;
}

if (is_array($data)) {
	$datax['code'] = 200;
	$datax['feed'] = $data;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Data Tidak Ditemukan";
}
echo encryptData($datax);

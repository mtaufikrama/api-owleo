<?php
include "../token/cek-no-token.php";

// id_jenis_produk, list_varian

$list_varian = [
    [
        "nama_varian" => "kombinasi",
        "varian_detail" => ["fitted", "reguler"],
    ],
    [
        "nama_varian" => "kombinasi",
        "varian_detail" => ["fitted", "reguler"],
    ],
];

if (empty($id_jenis_produk)) {
    $datax['code'] = 404;
    $datax['msg'] = "Jenis Produk Tidak Ditemukan";
    echo encryptData($datax);
    die();
}

$cekJenisProduk = baca_tabel("jenis_produk", "count(*)", "where id='$id_jenis_produk'");

if ($cekJenisProduk <= 0) {
    $datax['code'] = 404;
    $datax['msg'] = "Jenis Produk Tidak Ditemukan";
    echo encryptData($datax);
    die();
}

foreach ($list_varian as $varian) {
    $id_varian = generateID('varian', 'id');
    $dataVarian['id'] = $id_varian;
    $dataVarian['id_jenis_produk'] = $id_jenis_produk;
    $dataVarian['nama'] = $varian['nama_varian'];
    $result = insert_tabel('varian', $dataVarian);
    if ($result) {
        foreach ($varian['varian_detail'] as $varian_det) {
            $id_varian_detail = generateID('varian_detail', 'id');
            $dataVarianDetail['id'] = $id_varian_detail;
            $dataVarianDetail['id_jenis_produk'] = $id_jenis_produk;
            $dataVarianDetail['id_varian'] = $id_varian;
            $dataVarianDetail['nama'] = $varian_det;
            $result = insert_tabel('varian_detail', $dataVarianDetail);
        }
    }
}

if ($result) {
    $datax['code'] = 200;
    $datax['msg'] = 'Berhasil Mengupload Varian';
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Gagal Mengupload Varian";
}

echo encryptData($datax);

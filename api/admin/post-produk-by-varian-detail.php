<?php
include "../token/cek-token-admin.php";

// id, id_produk_by_jenis_produk, id_varian_detail

if (empty($id_produk_by_jenis_produk)) {
	$datax['code'] = 500;
	$datax['msg'] = "ID Produk By Jenis Produk tidak ada";
	echo encryptData($datax);
	die();
}

if (empty($id_varian_detail)) {
	$datax['code'] = 500;
	$datax['msg'] = "ID Varian Detail tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('produk_by_varian_detail', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id_produk_by_jenis_produk)) {
	unset($cekID);
	$cekID = baca_tabel('produk_by_jenis_produk', 'count(*)', "where binary id = '$id_produk_by_jenis_produk'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id_varian_detail)) {
	unset($cekID);
	$cekID = baca_tabel('varian_detail', 'count(*)', "where binary id = '$id_varian_detail'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID Varian Detail tidak ada";
		echo encryptData($datax);
		die();
	}
}

$dataProdukByVarianDetail['id_produk_by_jenis_produk'] = $id_produk_by_jenis_produk;
$dataProdukByVarianDetail['id_varian_detail'] = $id_varian_detail;
$dataProdukByVarianDetail['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$id = generateID('produk_by_varian_detail', 'id');
	$dataProdukByVarianDetail['id'] = $id;
	$result = insert_tabel('produk_by_varian_detail', $dataProdukByVarianDetail);
} else {
	$result = update_tabel('produk_by_varian_detail', $dataProdukByVarianDetail, "where binary id='$id'");
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Produk";
	$datax['id'] = $id;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Produk";
}
echo encryptData($datax);

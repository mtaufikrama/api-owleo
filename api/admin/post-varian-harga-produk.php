<?php
include "../token/cek-token-admin.php";

// id_produk, id_jenis_produk, list_kombinasi

// $list_kombinasi = [
//     [
//         "id" => "id_varian_kombinasi",
//         "varian" => ["id_produk_by_varian_detail", "id_produk_by_varian_detail"],
//         "harga" => 75000
//     ]
// ];

foreach ($list_kombinasi as $kombinasi) {
    $dataVarianKombinasi['id_jenis_produk'] = $id_jenis_produk;
    $dataVarianKombinasi['harga'] = $kombinasi['harga'];

    if (empty($kombinasi['id'])) {
        $dataVarianKombinasi['id'] = generateID('produk_by_varian_kombinasi', 'id');
        $result = insert_tabel('produk_by_varian_kombinasi', $dataVarianKombinasi);
    } else {
        $dataVarianKombinasi['id'] = $kombinasi['id'];
        $result = update_tabel('produk_by_varian_kombinasi', $dataVarianKombinasi, "where id='" . $dataVarianKombinasi['id'] . "'");
    }
    if ($result) {
        $id_produk_by_varian_kombinasi = $dataVarianKombinasi['id'];
        foreach ($kombinasi['varian'] as $id_produk_by_varian_detail) {
            $dataVarianKombinasiList['id_produk_by_varian_kombinasi'] = $id_produk_by_varian_kombinasi;
            $dataVarianKombinasiList['id_produk_by_varian_detail'] = $id_produk_by_varian_detail;
            $cek = baca_tabel('produk_by_varian_kombinasi_list', 'count(*)', "where id_produk_by_varian_kombinasi='$id_produk_by_varian_kombinasi' and id_produk_by_varian_detail='$id_produk_by_varian_detail'");
            if ($cek <= 0) {
                $id_produk_by_varian_kombinasi_list = generateID('produk_by_varian_kombinasi_list', 'id');
                $dataVarianKombinasiList['id'] = $id_produk_by_varian_kombinasi_list;
                if ($result) $result = insert_tabel('produk_by_varian_kombinasi_list', $dataVarianKombinasiList);
            }
        }
    }
}

if ($result) {
    $datax['code'] = 200;
    $datax['msg'] = 'Berhasil Mengupload Varian Kombinasi';
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Gagal Mengupload Varian Kombinasi";
}
echo encryptData($datax);

<?php
include "../token/cek-token-admin.php";

// id, id_produk_by_varian_detail, file, no_urut

if (empty($id_produk_by_varian_detail)) {
	$datax['code'] = 500;
	$datax['msg'] = "ID Produk tidak ada";
	echo encryptData($datax);
	die();
}

if (empty($file)) {
	if (!is_array($_FILES)) {
		$datax['code'] = 500;
		$datax['msg'] = "Foto tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id)) {
	$cekID = baca_tabel('produk_by_varian_detail_img', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($id_produk_by_varian_detail)) {
	unset($cekID);
	$cekID = baca_tabel('produk_by_varian_detail', 'count(*)', "where binary id = '$id_produk_by_varian_detail'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (!empty($file)) {
	$parsed_url = parse_url($file);
	$local_path = ltrim($parsed_url['path'], '/');
	$fileName = $local_path;
	$date = date("Y-m-d H:i:s");
	$result = true;
} else {
	$file = $_FILES['file'];

	if (!is_array($file)) {
		$datax['code'] = 500;
		$datax['msg'] = 'Tidak ada konten yang diinput';
		echo encryptData($datax);
		die();
	}

	if ($file['size'] >= 200000000) {
		$datax['code'] = 500;
		$datax['msg'] = 'Data Terlalu Besar';
		echo encryptData($datax);
		die();
	}

	$fileName = basename($file['name']);
	$date = date("Y-m-d H:i:s");
	$id_produk_by_jenis_produk = baca_tabel('produk_by_varian_detail', 'id_produk_by_jenis_produk', "where binary id = '$id_produk_by_varian_detail'");
	$id_produk = baca_tabel('produk_by_jenis_produk', 'id_produk', "where binary id = '$id_produk_by_jenis_produk'");
	$pathDir = "assets/product/$id_produk/$id_produk_by_jenis_produk/$id_produk_by_varian_detail/";
	$homeDir = __DIR__ . "/../../" . $pathDir;
	if (!is_dir($homeDir)) {
		$cekMkDir = mkdir($homeDir, 0777, true);
		if (!$cekMkDir) {
			$datax['code'] = 500;
			$datax['msg'] = "Tidak Berhasil Menambahkan Folder $pathDir";
			echo encryptData($datax);
			die();
		}
	}

	$result = move_uploaded_file($file['tmp_name'], $homeDir . $fileName);

	$fileName = $pathDir . $fileName;
}

$dataContent['id_user'] = $id_user;
$dataContent['id_produk_by_varian_detail'] = $id_produk_by_varian_detail;
$dataContent['path_image'] = $fileName;
$dataContent['waktu'] = $date;

if (empty($id)) {
	$id = generateID('produk_by_varian_detail_img', 'id');
	$dataContent['id'] = $id;
	if ($result) $result = insert_tabel('produk_by_varian_detail_img', $dataContent);
} else {
	if ($result) $result = update_tabel('produk_by_varian_detail_img', $dataContent, "where binary id='$id'");
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Konten Produk";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Konten Produk";
}

echo encryptData($datax);

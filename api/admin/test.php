<?php
$id_kota_origin = 154;
$id_kota_destination = 154;
$weight = 2000;

include '../function/api_rajaongkir.php';
$rajaongkir = new rajaongkir();

$list_ongkir = [];
$min_value = PHP_INT_MAX;
$list_courier = ['jne', 'pos', 'tiki'];
foreach ($list_courier as $courier) {
    $ongkir_courier = $rajaongkir->cost($id_kota_origin, $id_kota_destination, $weight, $courier);
    foreach ($ongkir_courier['rajaongkir']['results'] as $results) {
        foreach ($results['costs'] as $costs) {
            foreach ($costs['cost'] as $cost) {
                if ($cost['value'] < $min_value) {

                    $min_value = $cost['value'];

                    $dataOngkir['code'] = $results['code'];
                    $dataOngkir['name'] = $results['name'];
                    $dataOngkir['service'] = $costs['service'];
                    $dataOngkir['description'] = $costs['description'];
                    $dataOngkir['etd'] = $cost['etd'];
                    $dataOngkir['note'] = $cost['note'];
                    $diskon_ongkir = 20 / 100;
                    $value = $cost['value'] - ($diskon_ongkir * $cost['value']);
                    $harga_ongkir = floor($value / 1000) * 1000;
                    $dataOngkir['harga_ongkir'] = $harga_ongkir;
                    $dataOngkir['value'] = $cost['value'];
                }
            }
        }
    }
}

echo json_encode($dataOngkir);
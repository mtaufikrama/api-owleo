<?php
include "../token/cek-token-admin.php";

// id, nama, url

if (empty($nama)) {
	$datax['code'] = 500;
	$datax['msg'] = "Nama tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('sosmed', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

$datasosmed['nama'] = $nama;
$datasosmed['url'] = $url;
$datasosmed['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$cekNama = baca_tabel('sosmed', 'count(*)', "where nama='$nama'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "Nama $nama sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID('sosmed', 'id');
	$datasosmed['id'] = $id;
	$result = insert_tabel('sosmed', $datasosmed);
} else {
	$action = 'update';
	$result = update_tabel('sosmed', $datasosmed, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-sosmed', json_encode($datasosmed), $action);
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil $action Sosial Media";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal $action Sosial Media";
}
echo encryptData($datax);

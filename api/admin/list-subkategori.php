<?php
include "../token/cek-token-admin.php";

// id_kategori

if (empty($id_kategori)) {
	$datax['code'] = 404;
	$datax['msg'] = "Data Tidak Ditemukan";
	echo encryptData($datax);
	die();
}

$sql = "SELECT * FROM subkategori where binary id_kategori='$id_kategori' order by nama asc";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
	if ($get['is_validate'] != 1) {
		$get['is_validate'] = false;
	} else {
		$get['is_validate'] = true;
	}
	$subkategori[] = $get;
}

if (is_array($subkategori)) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
	$datax['subkategori'] = $subkategori;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Data Tidak Ditemukan";
}
echo encryptData($datax);

<?php
include "../token/cek-no-token.php";
// include "../token/cek-token-admin.php";

// id_produk, id_jenis_produk

function generateCombinations($variants, $index = 0, $combination = array(), &$combinations = array())
{
    // Jika kita sudah mencapai indeks varian terakhir
    if ($index === count($variants)) {
        $combinations[] = $combination; // Tambahkan kombinasi ke dalam daftar
        return;
    }

    // Ambil varian untuk indeks saat ini
    $currentVariantKey = array_keys($variants)[$index];
    $currentVariant = $variants[$currentVariantKey];

    // Loop melalui semua nilai varian untuk indeks saat ini
    foreach ($currentVariant as $value) {
        // Buat salinan kombinasi saat ini
        $currentCombination = $combination;
        // Tambahkan nilai varian ke dalam kombinasi saat ini
        $currentCombination[$currentVariantKey] = $value;
        // Rekursi untuk varian berikutnya
        generateCombinations($variants, $index + 1, $currentCombination, $combinations);
    }
}

// $db->debug = true;

$id_produk_by_jenis_produk = baca_tabel('produk_by_jenis_produk', 'id', "where binary id_produk='$id_produk' and binary id_jenis_produk='$id_jenis_produk'");

if (empty($id_produk_by_jenis_produk)) {
    $datax['code'] = 500;
    $datax['msg'] = 'Data Tidak Ditemukan';
    echo encryptData($datax);
    die();
}

$sql = "SELECT id_varian
FROM produk_by_varian_detail a
LEFT JOIN varian_detail b ON a.id_varian_detail=b.id
WHERE binary a.id_produk_by_jenis_produk='$id_produk_by_jenis_produk'
GROUP BY b.id_varian";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
    $id_varian = $get['id_varian'];
    $sqlVarianDetail = "SELECT a.id as id_produk_by_varian_detail, a.id_varian_detail
        FROM produk_by_varian_detail a
        LEFT JOIN varian_detail b ON a.id_varian_detail=b.id
        WHERE binary a.id_produk_by_jenis_produk='$id_produk_by_jenis_produk'
        AND binary b.id_varian='$id_varian'";

    $runVarianDetail = $db->Execute($sqlVarianDetail);

    while ($getVarianDetail = $runVarianDetail->fetchRow()) {
        $varianDetail[] = $getVarianDetail;
    }

    // $varian[$nama_varian] = $varianDetail;
    $varian[$id_varian] = $varianDetail;

    unset($varianDetail);

    // $list_varian[] = $varian;
}

// Hasilkan semua kombinasi
$allCombinations = array();
generateCombinations($varian, 0, array(), $allCombinations);

foreach ($allCombinations as $combination) {
    foreach ($combination as $key => $val) {
        $id_varian_detail = $val['id_varian_detail'];
        $list_id_varian_detail[] = $val['id_produk_by_varian_detail'];
        $nama_varian = baca_tabel('varian_detail a join varian b on a.id_varian=b.id', 'b.nama', "where a.id='$id_varian_detail'");
        $detail['nama_varian'] = $nama_varian;
        $detail['id_varian_detail'] = $id_varian_detail;
        $detail['nama_varian_detail'] = baca_tabel('varian_detail', 'nama', "where binary id='$id_varian_detail'");
        $list_nama_varian_detail[] = $detail;
    }
    $varian_detail['nama'] =  $list_nama_varian_detail;
    $varian_detail['varian'] = $list_id_varian_detail;

    $varian_detail_list = implode(',', array_map(function ($item) {
        return "'$item'";
    }, $list_id_varian_detail));

    $sqlVarianHarga = "SELECT DISTINCT b.id as id_produk_by_varian_kombinasi, b.harga
    FROM produk_by_varian_kombinasi_list a
    JOIN produk_by_varian_kombinasi b ON a.id_produk_by_varian_kombinasi = b.id
    WHERE a.id_produk_by_varian_kombinasi IN (
        SELECT a.id_produk_by_varian_kombinasi
        FROM produk_by_varian_kombinasi_list a
        WHERE a.id_produk_by_varian_detail IN ($varian_detail_list)
        GROUP BY a.id_produk_by_varian_kombinasi
        HAVING COUNT(DISTINCT a.id_produk_by_varian_detail) = " . count($list_id_varian_detail) . "
    )";

    $runVarianHarga = $db->Execute($sqlVarianHarga);

    while ($getVarianHarga = $runVarianHarga->fetchRow()) {
        $id_varian_kombinasi = $getVarianHarga['id_produk_by_varian_kombinasi'];
        $harga = $getVarianHarga['harga'];
        $varian_detail['id'] =  $id_varian_kombinasi;
        $varian_detail['harga'] = $harga;
    }

    // $varian_detail['id'] =  $id_varian_kombinasi;
    // $varian_detail['harga'] = $harga;

    unset($list_id_varian_detail);
    unset($list_nama_varian_detail);

    $datax[] = $varian_detail;
}

if (is_array($datax)) {
    $dataRes['code'] = 200;
    $dataRes['varian_harga'] = $datax;
} else {
    $dataRes['code'] = 500;
    $dataRes['msg'] = 'Data Tidak Ditemukan';
}

// echo json_encode($dataRes);
echo encryptData($dataRes);

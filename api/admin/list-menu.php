<?php
include "../token/cek-token-admin.php";

$id_roles = baca_tabel('user a join roles b on a.id_roles=b.id', 'b.nama_roles', "where binary a.id='$id_user'");

switch ($id_roles) {

	case 'admin':
		$menu = array(
			array(
				'nama' =>	'List User',
				'route' =>	'list-user',
			),
			array(
				'nama' =>	'List Email',
				'route' =>	'list-email',
			),
			array(
				'nama' =>	'List Kritik Saran',
				'route' =>	'list-kritik-saran',
			),
			array(
				'nama' =>	'List Kategori',
				'route' =>	'list-kategori',
			),
		);
		$datax['code'] = 200;
		$datax['menu'] = $menu;
		break;

	case 'superadmin':
		$menu = array(
			array(
				'nama' =>	'List User',
				'route' =>	'list-user',
			),
			array(
				'nama' =>	'List Email',
				'route' =>	'list-email',
			),
			array(
				'nama' =>	'List Kritik Saran',
				'route' =>	'list-kritik-saran',
			),
			array(
				'nama' =>	'List Kategori',
				'route' =>	'list-kategori',
			),
		);
		$datax['code'] = 200;
		$datax['menu'] = $menu;
		break;

	default:
		$datax['code'] = 500;
		$datax['msg'] = 'Hanya admin yang dapat mengakses';
		break;
}
echo encryptData($datax);

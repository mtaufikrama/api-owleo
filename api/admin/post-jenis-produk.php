<?php
include "../token/cek-token-admin.php";

// id, nama, deskripsi, berat

if (!empty($nama)) $dataJenisProduk['nama'] = $nama;
if (!empty($deskripsi)) $dataJenisProduk['deskripsi'] = $deskripsi;
if (empty($berat)) {
	$datax['code'] = 404;
	$datax['msg'] = "ID tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('jenis_produk', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (empty($id)) {
	$cekNama = baca_tabel('jenis_produk', 'count(*)', "where nama='$nama'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "jenis_produk $nama sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID('jenis_produk', 'id');
	$dataJenisProduk['id'] = $id;
	$result = insert_tabel('jenis_produk', $dataJenisProduk);
} else {
	$action = 'update';
	$result = update_tabel('jenis_produk', $dataJenisProduk, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-jenis_produk', json_encode($dataJenisProduk), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}

echo encryptData($datax);

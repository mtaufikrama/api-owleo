<?php
include "../token/cek-token-admin.php";

// id

if (empty($id)) {
	$datax['code'] = 404;
	$datax['msg'] = "ID tidak ada";
	echo encryptData($datax);
	die();
} else {
	$cekID = baca_tabel('jenis_produk', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ditemukan";
		echo encryptData($datax);
		die();
	}
}

$getData = get_all_data('jenis_produk', "where binary id='$id'");

$action = 'delete';

$cekProduk = baca_tabel('produk_by_jenis_produk', 'count(*)', "where binary id_jenis_produk='$id'");

if ($cekProduk > 0) {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal, karena jenis produk telah digunakan di beberapa produk";
	echo encryptData($datax);
	die();
}

$result = delete_tabel('jenis_produk', "where binary id='$id'");

if ($result) {
	activity_user($id_user, 'delete-jenis_produk', json_encode($getData), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}

echo encryptData($datax);

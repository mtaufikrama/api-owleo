<?php
include "../token/cek-token-admin.php";

// id, kata, ubah

if (empty($kata)) {
	$datax['code'] = 500;
	$datax['msg'] = "Kata tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('distract', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

$dataDistract['kata'] = $kata;
$dataDistract['ubah'] = $ubah;
$dataDistract['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$cekNama = baca_tabel('distract', 'count(*)', "where kata='$kata'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "kata $kata sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID('distract', 'id');
	$dataDistract['id'] = $id;
	$result = insert_tabel('distract', $dataDistract);
} else {
	$action = 'update';
	$result = update_tabel('distract', $dataDistract, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-distraction', json_encode($dataDistract), $action);
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil $action Distraction";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal $action Distraction";
}
echo encryptData($datax);

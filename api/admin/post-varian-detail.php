<?php
include "../token/cek-token-admin.php";

// id, id_varian, nama

if (!empty($id_varian)) $varianDetail['id_varian'] = $id_varian;
if (!empty($nama)) $varianDetail['nama'] = $nama;

if (!empty($id)) {
	$cekID = baca_tabel('varian_detail', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (empty($id)) {
	$cekNama = baca_tabel('varian_detail', 'count(*)', "where nama='$nama'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "Varian Detail $nama sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID('varian_detail', 'id');
	$varianDetail['id'] = $id;
	$result = insert_tabel('varian_detail', $varianDetail);
} else {
	$action = 'update';
	$result = update_tabel('varian_detail', $varianDetail, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-varian-detail', json_encode($varianDetail), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}
echo encryptData($datax);

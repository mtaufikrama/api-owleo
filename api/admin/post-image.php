<?php

header("Access-Control-Allow-Origin: *");

// file

if (empty($file)) {
	if (!is_array($_FILES)) {
		$datax['code'] = 500;
		$datax['msg'] = "Foto tidak ada";
		echo json_encode($datax);
		die();
	}
}

if (!empty($file)) {
	$parsed_url = parse_url($file);
	$local_path = ltrim($parsed_url['path'], '/');
	$fileName = $local_path;
	$date = date("Y-m-d H:i:s");
	$result = true;
} else {
	$file = $_FILES['file'];

	if (!is_array($file)) {
		$datax['code'] = 500;
		$datax['msg'] = 'Tidak ada konten yang diinput';
		echo json_encode($datax);
		die();
	}

	if ($file['size'] >= 200000000) {
		$datax['code'] = 500;
		$datax['msg'] = 'Data Terlalu Besar';
		echo json_encode($datax);
		die();
	}

	$fileName = $file['name'];
	$date = date("Y-m-d H:i:s");
	$pathDir = "assets/product/";
	$homeDir = __DIR__ . "/../../" . $pathDir;
	if (!is_dir($homeDir)) {
		$cekMkDir = mkdir($homeDir, 0777, true);
		if (!$cekMkDir) {
			$datax['code'] = 500;
			$datax['msg'] = "Tidak Berhasil Menambahkan Folder $pathDir";
			echo encryptData($datax);
			die();
		}
	}

	$result = move_uploaded_file($file['tmp_name'], $homeDir . $fileName);

	$fileName = $pathDir . $fileName;
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil Menambahkan Konten Produk";
	$datax['file_name'] = $fileName;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Menambahkan Konten Produk";
}

echo json_encode($datax);

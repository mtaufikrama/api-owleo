<?php
include "../token/cek-token-admin.php";

// id, id_kategori, nama

if (!empty($id_kategori)) $subkategori['id_kategori'] = $id_kategori;
if (!empty($nama)) $subkategori['nama'] = $nama;

$subkategori['is_validate'] = 1;

if (!empty($id)) {
	$cekID = baca_tabel('subkategori', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (empty($id)) {
	$cekNama = baca_tabel('subkategori', 'count(*)', "where nama='$nama'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "Sub-kategori $nama sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID('subkategori', 'id');
	$subkategori['id'] = $id;
	$result = insert_tabel('subkategori', $subkategori);
} else {
	$action = 'update';
	$result = update_tabel('subkategori', $subkategori, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-subkategori', json_encode($subkategori), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}
echo encryptData($datax);

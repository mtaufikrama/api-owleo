<?php
include "../token/cek-token-admin.php";

// id, caption, tabs, kode

$cekTabs = baca_tabel('tabs', 'count(*)', "where nama = '$tabs'");

if (!$tabs || $tabs == '' || $cekTabs == 0) {
	$datax['code'] = 404;
	$datax['msg'] = "Tab Tidak Terdaftar";
	echo encryptData($datax);
	die();
}

$id_tabs = baca_tabel('tabs', 'id', "where nama = '$tabs'");

if (empty($id)) {
	$id_feed = generateID('feed', 'id');
} else {
	$id_feed = $id;
}

if ($id_tabs == '1b2IDNZbMY5JJ0e') {
	$dataFeed['kode'] = $id_feed;
} else {
	$dataFeed['kode'] = $kode;
}

$dataFeed['id_tabs'] = $id_tabs;
$dataFeed['caption'] = $caption;
$dataFeed['id_user'] = $id_user;
$dataFeed['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$dataFeed['id'] = $id_feed;
	$result = insert_tabel('feed', $dataFeed);
} else {
	$result = update_tabel('feed', $dataFeed, "where id='$id'");
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil $action Feed";
	$datax['tabs'] = 'feed';
	$datax['kode'] = $id_feed;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal $action Feed";
}
echo encryptData($datax);

<?php
include "../token/cek-token-admin.php";

$id_produk = generateID('produk', 'id');

$dataProduk['id'] = $id_produk;
$dataProduk['id_user'] = $id_user;

if (is_array($list_varian)) {
    $datax['code'] = 200;
    $datax['varian'] = $list_varian;
} else {
    $datax['code'] = 500;
    $datax['msg'] = 'Data Tidak Ditemukan';
}

// echo json_encode($datax);
echo encryptData($datax);

<?php
include "../token/cek-token-admin.php";

// id

if (empty($id)) {
	$datax['code'] = 404;
	$datax['msg'] = "ID tidak ada";
	echo encryptData($datax);
	die();
} else {
	$cekID = baca_tabel('varian_detail', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ditemukan";
		echo encryptData($datax);
		die();
	}
}

$getData = get_all_data('varian_detail', "where binary id='$id'");

$action = 'delete';

$cekProduk = baca_tabel('produk_by_varian_detail', 'count(*)', "where binary id_varian_detail='$id'");

if ($cekProduk > 0) {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal, karena varian telah digunakan di beberapa produk";
	echo encryptData($datax);
	die();
}

$result = delete_tabel('varian_detail', "where binary id='$id'");

if ($result) {
	activity_user($id_user, 'delete-varian-detail', json_encode($getData), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}

echo encryptData($datax);

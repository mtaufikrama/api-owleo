<?php
include "../token/cek-token-admin.php";

// id

if (empty($id)) {
	$datax['code'] = 404;
	$datax['msg'] = "ID tidak ada";
	echo encryptData($datax);
	die();
} else {
	$cekID = baca_tabel('olshop', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ditemukan";
		echo encryptData($datax);
		die();
	}
}

$getData = get_all_data('olshop', "where binary id='$id'");

$action = 'delete';
$result = delete_tabel('olshop', "where binary id='$id'");

if ($result) {
	activity_user($id_user, 'delete-olshop', json_encode($getData), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}

echo encryptData($datax);
